import com.baran.daniel.dto.PlaneDto;
import com.baran.daniel.model.Coordinates;
import com.baran.daniel.model.FlightDynamics;
import com.baran.daniel.model.NavigationGrid;
import com.baran.daniel.service.collision.DetectionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DetectionServiceTest {

    @Mock
    private NavigationGrid navigationGrid;

    @Mock
    private DetectionService detectionService;

    @BeforeEach
    public void setup() {
        detectionService = new DetectionService(navigationGrid);
    }

    @Test
    void testDetectingPotentialCollision() {
        PlaneDto currentPlane = PlaneDto.builder()
                .id(0)
                .coordinates(new Coordinates(500, 2000, 3000))
                .dynamics(new FlightDynamics(100, 0, 1000, Math.toRadians(0)))
                .build();

        PlaneDto nearbyPlane = PlaneDto.builder()
                .id(1)
                .coordinates(new Coordinates(480, 2000, 3000))
                .dynamics(new FlightDynamics(100, 0, 1000, Math.toRadians(0)))
                .build();

        when(navigationGrid.getSurroundingPlanes(currentPlane)).thenReturn(List.of(nearbyPlane));

        boolean collisionDetected = detectionService.hasPotentialCollision(currentPlane);
        assertTrue(collisionDetected, "Should detect a potential collision");
    }

    @Test
    void testNoCollisionDetected() {
        PlaneDto currentPlane = PlaneDto.builder()
                .id(0)
                .coordinates(new Coordinates(500, 2000, 3000))
                .dynamics(new FlightDynamics(100, 0, 1000, Math.toRadians(100)))
                .build();
        PlaneDto distantPlane = PlaneDto.builder()
                .id(1)
                .coordinates(new Coordinates(200, 2000, 3000))
                .dynamics(new FlightDynamics(100, 0, 1000, Math.toRadians(100)))
                .build();

        when(navigationGrid.getSurroundingPlanes(currentPlane)).thenReturn(List.of(distantPlane));

        boolean collisionDetected = detectionService.hasPotentialCollision(currentPlane);
        assertFalse(collisionDetected, "Should not detect a collision");
    }
}
