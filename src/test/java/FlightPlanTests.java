import com.baran.daniel.model.Coordinates;
import com.baran.daniel.model.FlightEvents;
import com.baran.daniel.model.FlightPlan;
import com.baran.daniel.model.LandingDetails;
import com.baran.daniel.model.Waypoint;
import com.baran.daniel.utility.FlightPlanHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;


class FlightPlanTests {

    @Test
    void shouldReturnRightCorridorFromNextWaypoint() {
        int corridorZero = FlightPlanHelper.getCorridorFromWaypoint(Waypoint.CHECKPOINT_EAST_0);
        int corridorZeroWest = FlightPlanHelper.getCorridorFromWaypoint(Waypoint.CHECKPOINT_WEST_0);
        int corridorOne = FlightPlanHelper.getCorridorFromWaypoint(Waypoint.CHECKPOINT_EAST_1);
        int corridorOneWest = FlightPlanHelper.getCorridorFromWaypoint(Waypoint.CHECKPOINT_WEST_1);

        assertEquals(0, corridorZero);
        assertEquals(0, corridorZeroWest);
        assertEquals(1, corridorOne);
        assertEquals(1, corridorOneWest);
    }


    @ParameterizedTest
    @MethodSource("startingWaypointDataset")
    void shouldReturnCorrectStartingWaypoint(Coordinates coordinates, Waypoint expectedWaypoint) {
        assertEquals(expectedWaypoint, FlightPlanHelper.getStartingWaypoint(coordinates));
    }

    private static Stream<Arguments> startingWaypointDataset() {
        return Stream.of(
                Arguments.of(new Coordinates(3000, 0, 4500), Waypoint.CORNER_SW_4),
                Arguments.of(new Coordinates(8000, 0, 4500), Waypoint.CORNER_SW_4),
                Arguments.of(new Coordinates(0, 1000, 4500), Waypoint.CORNER_SW_4),
                Arguments.of(new Coordinates(0, 0, 4500), Waypoint.CORNER_SW_4),
                Arguments.of(new Coordinates(0, 1001, 4500), Waypoint.CORNER_NW_4),
                Arguments.of(new Coordinates(0, 9999, 4500), Waypoint.CORNER_NW_4),
                Arguments.of(new Coordinates(1000, 9999, 4500), Waypoint.CORNER_NW_4),
                Arguments.of(new Coordinates(1001, 9999, 4500), Waypoint.CORNER_NE_4),
                Arguments.of(new Coordinates(1001, 9999, 4500), Waypoint.CORNER_NE_4),
                Arguments.of(new Coordinates(9999, 999, 4500), Waypoint.CORNER_SE_4)
        );
    }

    @Test
    void test() {
        FlightPlan flightPlan = new FlightPlan(new Coordinates(9999, 999, 4500));
        FlightEvents events = FlightEvents.builder().build();
        LandingDetails landingDetails = new LandingDetails();
//        flightPlan.getNextWaypoint();
    }

    @ParameterizedTest
    @MethodSource("waypointAfterCheckpointDataset")
    public void testGetWaypointAfterCheckpoint(Waypoint waypoint, boolean hasPermission, Waypoint expectedNextWaypoint) {
        assertEquals(expectedNextWaypoint, FlightPlanHelper.getWaypointAfterCheckpoint(waypoint, hasPermission));
    }

    private static Stream<Arguments> waypointAfterCheckpointDataset() {
        return Stream.of(
                Arguments.of(Waypoint.CHECKPOINT_WEST_0, true, Waypoint.RUNWAY_END_0),
                // Add more test cases for other waypoints and permission scenarios
        );
    }
}
