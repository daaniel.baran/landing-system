package helper;

import com.baran.daniel.model.Coordinates;
import com.baran.daniel.utility.CoordinatesHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class CoordinatesHelperTest {

    @Test
    void testDistanceBetweenTwoCoordinates3D() {
        Coordinates coordinates1 = new Coordinates(2000, 2000, 4000);
        Coordinates coordinates2 = new Coordinates(2000, 2000, 3900);
        double expectedDistance = 100;
        double distance = CoordinatesHelper.distanceBetweenTwoCoordinates3D(coordinates1, coordinates2);
        assertEquals(expectedDistance, distance, "Distance calculation is correct");
    }

    @Test
    void testDistanceBetweenTwoCoordinates2D() {
        Coordinates coordinates1 = new Coordinates(2000, 2000, 4000);
        Coordinates coordinates2 = new Coordinates(2000, 1000, 3000);
        double expectedDistance = 1000;
        double distance = CoordinatesHelper.distanceBetweenTwoCoordinates2D(coordinates1, coordinates2);
        assertEquals(expectedDistance, distance, "Distance calculation is correct");
    }

    @Test
    void testCalculateYCoordinateByHeading() {
        double initialY = 1000;
        double heading = Math.toRadians(90);
        double velocity = 100;
        double expectedY = 1100;
        double newY = CoordinatesHelper.calculateYCoordinateByHeading(initialY, heading, velocity);
        assertEquals(expectedY, newY, "Y-coordinate calculation is incorrect");
    }

    @Test
    void testCalculateXCoordinateByHeading() {
        double initialX = 2000;
        double heading = Math.toRadians(180);
        double velocity = 100;
        double expectedX = 1900;
        double newX = CoordinatesHelper.calculateXCoordinateByHeading(initialX, heading, velocity);
        assertEquals(expectedX, newX, "X-coordinate calculation is incorrect");
    }

    @Test
    void testInvalidCoordinates() {
        Coordinates coords = new Coordinates(-1000, 2000, 3000);
        boolean expectedResult = true;
        boolean result = CoordinatesHelper.invalidCoordinates(coords);
        assertEquals(expectedResult, result, "Coordinates validation is incorrect");
    }

    @ParameterizedTest
    @MethodSource("edgeTestData")
    void testIdentifyEdge(Coordinates coordinates, CoordinatesHelper.Edge expectedEdge) {
        CoordinatesHelper.Edge result = CoordinatesHelper.identifyEdge(coordinates);
        assertEquals(expectedEdge, result, "Edge identification is incorrect");
    }

    private static Stream<Arguments> edgeTestData() {
        return Stream.of(
                arguments(new Coordinates(0, 1000, 3000), CoordinatesHelper.Edge.WEST),
                arguments(new Coordinates(9999, 1000, 3000), CoordinatesHelper.Edge.EAST),
                arguments(new Coordinates(1000, 0, 2000), CoordinatesHelper.Edge.SOUTH),
                arguments(new Coordinates(2000, 9999, 2000), CoordinatesHelper.Edge.NORTH)
        );
    }
}
