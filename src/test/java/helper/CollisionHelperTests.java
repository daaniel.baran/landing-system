package helper;

import com.baran.daniel.dto.PlaneDto;
import com.baran.daniel.model.Coordinates;
import com.baran.daniel.model.FlightDynamics;
import com.baran.daniel.utility.CollisionHelper;
import com.baran.daniel.utility.CollisionHelper.CollisionDirection;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CollisionHelperTests {

    private final PlaneDto plane = createTestingPlane();
    private final PlaneDto planeTwo = createTestingSecondPlane();

    @Test
    void shouldReturnCorrectDirection() {
        CollisionDirection backAsExpectedDirection = CollisionDirection.BACK;
        CollisionDirection frontAsExpectedDirection = CollisionDirection.FRONT;

        CollisionDirection back = CollisionHelper.getCollisionDirection(135d);
        CollisionDirection right = CollisionHelper.getCollisionDirection(134d);
        CollisionDirection left = CollisionHelper.getCollisionDirection(226d);
        CollisionDirection front = CollisionHelper.getCollisionDirection(315d);

        assertEquals(backAsExpectedDirection, back);
        assertEquals(frontAsExpectedDirection, front);
        assertNotEquals(backAsExpectedDirection, right);
        assertNotEquals(frontAsExpectedDirection, left);
    }

    @Test
    void shouldThrowExceptionForIncorrectBearing() {
        assertThrows(IllegalArgumentException.class, () -> CollisionHelper.getCollisionDirection(135231d));
    }

    @Test
    void shouldReturnCorrectBearing() {
        Double bearing = CollisionHelper.calculateRelativeBearingInDegrees(plane, planeTwo);
        Double expectedBearing = 180d;
        assertEquals(expectedBearing, bearing);
        assertNotEquals(181d, expectedBearing);
    }

    private PlaneDto createTestingPlane() {
        return PlaneDto.builder()
                .id(0)
                .coordinates(new Coordinates(2000, 5000, 4500))
                .dynamics(new FlightDynamics(100, 0, 1000, Math.toRadians(90)))
                .build();
    }

    private PlaneDto createTestingSecondPlane() {
        return PlaneDto.builder()
                .id(0)
                .coordinates(new Coordinates(2000, 4800, 4500))
                .dynamics(new FlightDynamics(100, 0, 1000, Math.toRadians(90)))
                .build();
    }
}
