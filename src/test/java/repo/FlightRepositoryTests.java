package repo;

import com.baran.daniel.config.DatabaseConnection;
import com.baran.daniel.db.FlightRepo;
import com.baran.daniel.model.FlightState.FlightStatus;
import jooq.model.tables.records.FlightRecord;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static jooq.model.tables.Flight.FLIGHT;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class FlightRepositoryTests {

    @Mock
    FlightRepo planeRepo = FlightRepo.getInstance();
    SQLDialect DIALECT = SQLDialect.POSTGRES;
    DSLContext CONTEXT = DSL.using(DatabaseConnection.getConnection(), DIALECT);

    @BeforeEach
    void setUp() {
        planeRepo.createPlane(0);
    }

    @Test
    void shouldReturnNewPlaneSavedToDatabase() {
        FlightRecord flight = CONTEXT.fetchOne(FLIGHT, FLIGHT.ID.eq(0L));
        assertNotNull(flight);
    }

    @Test
    void shouldReturnStatusLanded() {
        planeRepo.setFlightAsLanded(0L);
        FlightRecord flight = CONTEXT.fetchOne(FLIGHT, FLIGHT.ID.eq(0L));
        if (flight != null)
            assertEquals(FlightStatus.ENDED.name(), flight.getFlightStatus());
    }

    @Test
    void shouldReturnStatusCrashed() {
        planeRepo.setFlightAsCrashed(0L);
        FlightRecord flight = CONTEXT.fetchOne(FLIGHT, FLIGHT.ID.eq(0L));
        if (flight != null)
            assertEquals(FlightStatus.CRASHED.name(), flight.getFlightStatus());
    }

    @Test
    void shouldReturnStatusRejected() {
        planeRepo.setFlightAsRejected(0L);
        FlightRecord flight = CONTEXT.fetchOne(FLIGHT, FLIGHT.ID.eq(0L));
        if (flight != null)
            assertEquals(FlightStatus.REJECTED.name(), flight.getFlightStatus());
    }
}
