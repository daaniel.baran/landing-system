CREATE TABLE flight
(
    id            BIGINT PRIMARY KEY,
    start_time    TIMESTAMP   default now(),
    end_time      TIMESTAMP,
    flight_status VARCHAR(20) default 'NEW'
);

