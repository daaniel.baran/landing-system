CREATE TABLE collision
(
    id             BIGINT    DEFAULT nextval(collision_id_seq) PRIMARY KEY,
    plane_id       INT,
    time           TIMESTAMP default now()
);

CREATE SEQUENCE collision_id_seq
    INCREMENT 1
    START 0;
