/*
 * This file is generated by jOOQ.
 */
package jooq.model.tables;


import java.time.LocalDateTime;
import java.util.function.Function;

import jooq.model.Keys;
import jooq.model.Public;
import jooq.model.tables.records.FlightRecord;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Function4;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Records;
import org.jooq.Row4;
import org.jooq.Schema;
import org.jooq.SelectField;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Flight extends TableImpl<FlightRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>public.flight</code>
     */
    public static final Flight FLIGHT = new Flight();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<FlightRecord> getRecordType() {
        return FlightRecord.class;
    }

    /**
     * The column <code>public.flight.id</code>.
     */
    public final TableField<FlightRecord, Long> ID = createField(DSL.name("id"), SQLDataType.BIGINT.nullable(false), this, "");

    /**
     * The column <code>public.flight.start_time</code>.
     */
    public final TableField<FlightRecord, LocalDateTime> START_TIME = createField(DSL.name("start_time"), SQLDataType.LOCALDATETIME(6).defaultValue(DSL.field(DSL.raw("now()"), SQLDataType.LOCALDATETIME)), this, "");

    /**
     * The column <code>public.flight.end_time</code>.
     */
    public final TableField<FlightRecord, LocalDateTime> END_TIME = createField(DSL.name("end_time"), SQLDataType.LOCALDATETIME(6), this, "");

    /**
     * The column <code>public.flight.flight_status</code>.
     */
    public final TableField<FlightRecord, String> FLIGHT_STATUS = createField(DSL.name("flight_status"), SQLDataType.VARCHAR(20).defaultValue(DSL.field(DSL.raw("'NEW'::character varying"), SQLDataType.VARCHAR)), this, "");

    private Flight(Name alias, Table<FlightRecord> aliased) {
        this(alias, aliased, null);
    }

    private Flight(Name alias, Table<FlightRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased <code>public.flight</code> table reference
     */
    public Flight(String alias) {
        this(DSL.name(alias), FLIGHT);
    }

    /**
     * Create an aliased <code>public.flight</code> table reference
     */
    public Flight(Name alias) {
        this(alias, FLIGHT);
    }

    /**
     * Create a <code>public.flight</code> table reference
     */
    public Flight() {
        this(DSL.name("flight"), null);
    }

    public <O extends Record> Flight(Table<O> child, ForeignKey<O, FlightRecord> key) {
        super(child, key, FLIGHT);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : Public.PUBLIC;
    }

    @Override
    public UniqueKey<FlightRecord> getPrimaryKey() {
        return Keys.FLIGHT_PKEY;
    }

    @Override
    public Flight as(String alias) {
        return new Flight(DSL.name(alias), this);
    }

    @Override
    public Flight as(Name alias) {
        return new Flight(alias, this);
    }

    @Override
    public Flight as(Table<?> alias) {
        return new Flight(alias.getQualifiedName(), this);
    }

    /**
     * Rename this table
     */
    @Override
    public Flight rename(String name) {
        return new Flight(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Flight rename(Name name) {
        return new Flight(name, null);
    }

    /**
     * Rename this table
     */
    @Override
    public Flight rename(Table<?> name) {
        return new Flight(name.getQualifiedName(), null);
    }

    // -------------------------------------------------------------------------
    // Row4 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row4<Long, LocalDateTime, LocalDateTime, String> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Function)}.
     */
    public <U> SelectField<U> mapping(Function4<? super Long, ? super LocalDateTime, ? super LocalDateTime, ? super String, ? extends U> from) {
        return convertFrom(Records.mapping(from));
    }

    /**
     * Convenience mapping calling {@link SelectField#convertFrom(Class,
     * Function)}.
     */
    public <U> SelectField<U> mapping(Class<U> toType, Function4<? super Long, ? super LocalDateTime, ? super LocalDateTime, ? super String, ? extends U> from) {
        return convertFrom(toType, Records.mapping(from));
    }
}
