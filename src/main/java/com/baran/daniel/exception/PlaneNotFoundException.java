package com.baran.daniel.exception;

public class PlaneNotFoundException extends Exception {

    public PlaneNotFoundException(long planeId) {
        super("Plane with id:" + planeId + " not found!");
    }
}
