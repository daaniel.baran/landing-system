package com.baran.daniel.exception;

public class NoMessageException extends Exception {

    public NoMessageException() {
        super("No message was received");
    }
}
