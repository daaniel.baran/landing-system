package com.baran.daniel.observer;

import com.baran.daniel.dto.PlaneDto;

public interface PlaneObserver {

    void updatePlane(PlaneDto plane);

    void removePlane(PlaneDto plane);
}
