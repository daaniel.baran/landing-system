package com.baran.daniel.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
    static final Logger LOG = LoggerFactory.getLogger("DatabaseConnection");

    private static class Constants {
        private static final String URL = PropertyLoader.getDatabaseUrl();
        private static final String USERNAME = PropertyLoader.getDataBaseUser();
        private static final String PASSWORD = PropertyLoader.getDatabasePassword();
    }

    private DatabaseConnection() {
    }

    public static Connection getConnection() {
        try {
            return DriverManager.getConnection(Constants.URL, Constants.USERNAME, Constants.PASSWORD);
        } catch (SQLException e) {
            LOG.info("Exception while trying to get connection {}", e.getMessage());
        }
        return null;
    }
}
