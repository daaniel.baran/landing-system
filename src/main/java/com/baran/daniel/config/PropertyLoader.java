package com.baran.daniel.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public class PropertyLoader {


    private static class Constants {
        private static final String ROOT_PATH = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource("")).getPath();
        private static final String DB_CONFIG_PATH = ROOT_PATH + "application.properties";
    }

    private static final Logger LOG = LoggerFactory.getLogger("PropertiesLoader");
    private static final Properties properties = new Properties();

    private PropertyLoader() {
    }

    static {
        try (FileInputStream inputStream = new FileInputStream(Constants.DB_CONFIG_PATH)) {
            properties.load(inputStream);
        } catch (IOException e) {
            LOG.error("Error loading properties {}", e.getMessage());
        }
    }

    public static String getProperty(String key) {
        return properties.getProperty(key);
    }

    public static int getCruisingVelocity() {
        return Integer.parseInt(getProperty("plane.cruising-velocity"));
    }

    public static int getLandingVelocity() {
        return Integer.parseInt(getProperty("plane.landing-velocity"));
    }

    public static int getMinCruisingAltitude() {
        return Integer.parseInt(getProperty("airport.min-cruising-altitude"));
    }

    public static double getMaxTurnRate() {
        return Math.toRadians(Double.parseDouble(getProperty("plane.max-turn-rate")));
    }

    public static long getMaxFuel() {
        return Long.parseLong(getProperty("plane.fuel"));
    }

    public static int getMaxCapacity() {
        return Integer.parseInt(getProperty("airport.max-capacity"));
    }

    public static int getPort() {
        return Integer.parseInt(getProperty("server.port"));
    }

    public static String getHost() {
        return getProperty("server.host");
    }

    public static String getDatabasePassword() {
        return getProperty("database.password");
    }

    public static String getDataBaseUser() {
        return getProperty("database.username");
    }

    public static String getDatabaseUrl() {
        return getProperty("database.url");
    }

    public static int getLowerBound() {
        return Integer.parseInt(getProperty("airport.lower-bound"));
    }

    public static int getHigherBound() {
        return Integer.parseInt(getProperty("airport.higher-bound"));
    }

    public static int getMinSafeDistance() {
        return Integer.parseInt(getProperty("plane.min-safe-distance"));
    }

    public static int getTimeDelayInMillis() {
        return Integer.parseInt(getProperty("plane.time-delay-in-millis"));
    }

    public static int getAirspaceHeight() {
        return Integer.parseInt(getProperty("grid.height"));
    }

    public static int getAirspaceWidth() {
        return Integer.parseInt(getProperty("grid.width"));
    }

    public static int getAirspaceLength() {
        return Integer.parseInt(getProperty("grid.length"));
    }

    public static int getWaypointRadius() {
        return Integer.parseInt(getProperty("grid.waypoint-radius"));
    }

    public static int getAirspaceCellSize() {
        return Integer.parseInt(getProperty("grid.cell-size"));
    }

    public static int getMaxAltitudeAdjustment() {
        return Integer.parseInt(getProperty("plane.max-altitude-adjustment"));
    }

    public  static int getEmergencyTurnRate() {
        return Integer.parseInt(getProperty("plane.emergency-turn-rate"));
    }
    public static int getMaxVelocityAdjustment() {
        return Integer.parseInt(getProperty("plane.max-velocity-adjustment"));
    }

    public static boolean isRunAnimationOn() {
        return Boolean.parseBoolean(getProperty("animation"));
    }

    public static int getPlanesForTesting() {
        return Integer.parseInt(getProperty("airport.testing-planes"));
    }
}
