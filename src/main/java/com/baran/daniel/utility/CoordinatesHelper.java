package com.baran.daniel.utility;

import com.baran.daniel.config.PropertyLoader;
import com.baran.daniel.model.Coordinates;

import static com.baran.daniel.utility.CoordinatesHelper.Edge.*;

public class CoordinatesHelper {

    public enum Edge {
        WEST, SOUTH, EAST, NORTH
    }

    private static class Constants {
        private static final int MIN_Y = 0;
        private static final int MAX_Y = PropertyLoader.getAirspaceWidth();
        private static final int MIN_X = 0;
        private static final int MAX_X = PropertyLoader.getAirspaceLength();
        private static final int AIRSPACE_DIMENSION_X = PropertyLoader.getAirspaceLength();
        private static final int AIRSPACE_DIMENSION_Y = PropertyLoader.getAirspaceWidth();
        private static final int AIRSPACE_DIMENSION_Z = PropertyLoader.getAirspaceHeight();
        private static final int CELL_SIZE = PropertyLoader.getAirspaceCellSize();
        private static final int CELLS_X = AIRSPACE_DIMENSION_X / CELL_SIZE;
        private static final int CELLS_Y = AIRSPACE_DIMENSION_Y / CELL_SIZE;
        private static final int CELLS_Z = AIRSPACE_DIMENSION_Z / CELL_SIZE;
    }

    private CoordinatesHelper() {
    }

    public static double distanceBetweenTwoCoordinates3D(Coordinates one, Coordinates two) {
        double distanceX = two.getX() - one.getX();
        double distanceY = two.getY() - one.getY();
        double distanceZ = two.getZ() - one.getZ();
        return Math.sqrt(distanceX * distanceX + distanceY * distanceY + distanceZ * distanceZ);
    }

    public static double distanceBetweenTwoCoordinates2D(Coordinates one, Coordinates two) {
        double distanceX = two.getX() - one.getX();
        double distanceY = two.getY() - one.getY();
        return Math.sqrt(distanceX * distanceX + distanceY * distanceY);
    }

    public static double calculateYCoordinateByHeading(double y, double currentHeading, double velocity) {
        double newY = y + (velocity * Math.sin(currentHeading));
        return Math.max(Constants.MIN_Y, Math.min(newY, Constants.MAX_Y));
    }

    public static double calculateXCoordinateByHeading(double x, double currentHeading, double velocity) {
        double newX = x + (velocity * Math.cos(currentHeading));
        return Math.max(Constants.MIN_X, Math.min(newX, Constants.MAX_X));
    }

    public static boolean belongsToCell(int xi, int yi, int zi) {
        return xi >= 0 && xi < Constants.CELLS_X &&
                yi >= 0 && yi < Constants.CELLS_Y &&
                zi >= 0 && zi < Constants.CELLS_Z;
    }

    public static boolean invalidCoordinates(Coordinates newCoords) {
        return newCoords.getX() < 0 || newCoords.getX() > Constants.AIRSPACE_DIMENSION_X ||
                newCoords.getY() < 0 || newCoords.getY() > Constants.AIRSPACE_DIMENSION_Y ||
                newCoords.getZ() < 0 || newCoords.getZ() > Constants.AIRSPACE_DIMENSION_Z;
    }

    public static Edge identifyEdge(Coordinates planeCoordinates) {
        if (planeCoordinates.getX() == Constants.MIN_X)
            return WEST;
        else if (planeCoordinates.getX() == Constants.MAX_X)
            return EAST;
        else if (planeCoordinates.getY() == Constants.MIN_Y)
            return SOUTH;
        else if (planeCoordinates.getY() == Constants.MAX_Y)
            return NORTH;
        else
            throw new IllegalArgumentException("Plane is not on a valid edge.");
    }
}
