package com.baran.daniel.utility;

import org.jooq.codegen.GenerationTool;

import java.nio.file.Files;
import java.nio.file.Path;

public class JooqHelper {

    public static void generateJooqStructure() throws Exception {
        GenerationTool.generate(
                Files.readString(
                        Path.of("jooq-config.xml")
                )
        );
    }

    public static void main(String[] args) throws Exception {
        generateJooqStructure();
    }
}
