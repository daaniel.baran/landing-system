package com.baran.daniel.utility;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonParser {

    private JsonParser() {
    }

    public static String to(Object object) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        return gson.toJson(object);
    }

    public static <T> T from(String message, Class<T> classType) {
        return new Gson().fromJson(message, classType);
    }

}
