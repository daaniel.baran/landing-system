package com.baran.daniel.utility;

import com.baran.daniel.model.Coordinates;
import com.baran.daniel.service.Plane;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class LogsHelper {

    private LogsHelper() {
    }

    public static String logPlanePosition(Plane plane) {
        Coordinates coordinates = plane.getState().getCoordinates();
        return "|id:"
                + plane.getId() + " | "
                + printCoordinates(coordinates) + " | "
                + plane.getFlightPlan().getCurrentWaypoint() + " | "
                + plane.getDynamics().getFuel() + " | "
                + printAltitudeChange(plane.getDynamics().getAltitudeChange()) + " | "
                + printVelocity(plane.getDynamics().getVelocity()) + " | "
                + printHeadingInDegrees(plane.getDynamics().getCurrentHeading()) + " |";
    }

    private static String printAltitudeChange(double altitudeChange) {
        return BigDecimal.valueOf(altitudeChange).setScale(2, RoundingMode.HALF_UP) + "m";
    }

    private static String printHeadingInDegrees(double heading) {
        return BigDecimal.valueOf(Math.toDegrees(heading)).setScale(2, RoundingMode.HALF_UP) + "°";
    }

    private static String printVelocity(int velocity) {
        return velocity + "m/s";
    }

    private static String printCoordinates(Coordinates coordinates) {
        return String.format("%.2f - %.2f - %.2f", coordinates.getX(), coordinates.getY(), coordinates.getZ());
    }

    public static String printDistanceInMeters(double distance){
        return BigDecimal.valueOf(distance).setScale(2, RoundingMode.HALF_UP) + "m";
    }
}


