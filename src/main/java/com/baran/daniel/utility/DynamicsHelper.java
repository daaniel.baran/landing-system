package com.baran.daniel.utility;

import com.baran.daniel.config.PropertyLoader;
import com.baran.daniel.model.Coordinates;

public class DynamicsHelper {

    private DynamicsHelper() {
    }

    private static class Constants {
        private static final double MAX_TURN_RATE = PropertyLoader.getMaxTurnRate();
    }

    public static double calculateRequiredHeadingInRadians(Coordinates coordinates1, Coordinates coordinates2) {
        double x1 = coordinates1.getX();
        double y1 = coordinates1.getY();
        double x2 = coordinates2.getX();
        double y2 = coordinates2.getY();
        return Math.atan2(y2 - y1, x2 - x1);
    }

    public static double calculateMaxTurn(double headingDifference) {
        return Math.signum(headingDifference) * Math.min(Math.abs(headingDifference), Constants.MAX_TURN_RATE);
    }

    public static double normalizeAngle(double angle) {
        if (angle > Math.PI) {
            angle -= 2 * Math.PI;
        } else if (angle < -Math.PI) {
            angle += 2 * Math.PI;
        }
        return angle;
    }

}
