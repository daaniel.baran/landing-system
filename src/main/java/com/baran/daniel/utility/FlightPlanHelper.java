package com.baran.daniel.utility;

import com.baran.daniel.config.PropertyLoader;
import com.baran.daniel.model.Coordinates;
import com.baran.daniel.model.Waypoint;

public class FlightPlanHelper {

    private static final int LOWER_BOUND = PropertyLoader.getLowerBound();
    private static final int UPPER_BOUND = PropertyLoader.getHigherBound();

    private FlightPlanHelper() {
    }

    public static int getCorridorFromWaypoint(Waypoint waypoint) {
        return switch (waypoint) {
            case CHECKPOINT_EAST_0, CHECKPOINT_WEST_0 -> 0;
            case CHECKPOINT_EAST_1, CHECKPOINT_WEST_1 -> 1;
            default -> throw new IllegalStateException("No corridor associated with waypoint: " + waypoint);
        };
    }

    public static Waypoint getStartingWaypoint(Coordinates coordinates) {
        CoordinatesHelper.Edge edge = CoordinatesHelper.identifyEdge(coordinates);
        return switch (edge) {
            case WEST -> coordinates.getY() <= LOWER_BOUND ? Waypoint.CORNER_SW_4 : Waypoint.CORNER_NW_4;
            case SOUTH -> coordinates.getX() >= UPPER_BOUND ? Waypoint.CORNER_SE_4 : Waypoint.CORNER_SW_4;
            case EAST -> coordinates.getY() >= UPPER_BOUND ? Waypoint.CORNER_NE_4 : Waypoint.CORNER_SE_4;
            case NORTH -> coordinates.getX() <= LOWER_BOUND ? Waypoint.CORNER_NW_4 : Waypoint.CORNER_NE_4;
        };
    }

    public static Waypoint getWaypointAfterCheckpoint(Waypoint waypoint, boolean hasPermission) {
        return switch (waypoint) {
            case CHECKPOINT_WEST_0 -> hasPermission ? Waypoint.RUNWAY_END_0 : Waypoint.CHECKPOINT_WEST_1;
            case CHECKPOINT_WEST_1 -> hasPermission ? Waypoint.RUNWAY_END_1 : Waypoint.CORNER_NW_1;
            case CHECKPOINT_EAST_1 -> hasPermission ? Waypoint.RUNWAY_START_1 : Waypoint.CHECKPOINT_EAST_0;
            case CHECKPOINT_EAST_0 -> hasPermission ? Waypoint.RUNWAY_START_0 : Waypoint.CORNER_SE_1;
            default -> throw new IllegalStateException("Unexpected waypoint: " + waypoint);
        };
    }

    public static Waypoint getWaypointAfterCornerOnSecondCircle(Waypoint waypoint) {
        return switch (waypoint) {
            case CORNER_SW_4 -> Waypoint.CORNER_NW_3;
            case CORNER_NW_4 -> Waypoint.CORNER_NE_3;
            case CORNER_NE_4 -> Waypoint.CORNER_SE_3;
            case CORNER_SE_4 -> Waypoint.CORNER_SW_3;

            case CORNER_SW_3 -> Waypoint.CORNER_NW_2;
            case CORNER_NW_3 -> Waypoint.CORNER_NE_2;
            case CORNER_NE_3 -> Waypoint.CORNER_SE_2;
            case CORNER_SE_3 -> Waypoint.CORNER_SW_2;

            case CORNER_SW_2 -> Waypoint.CORNER_NW_1;
            case CORNER_NW_2 -> Waypoint.CORNER_NE_1;
            case CORNER_NE_2 -> Waypoint.CORNER_SE_1;
            case CORNER_SE_2 -> Waypoint.CORNER_SW_1;

            case CORNER_SW_1 -> Waypoint.CHECKPOINT_WEST_0;
            case CORNER_NW_1 -> Waypoint.CORNER_NE_1;
            case CORNER_NE_1 -> Waypoint.CHECKPOINT_EAST_1;
            case CORNER_SE_1 -> Waypoint.CORNER_SW_1;
            default -> throw new IllegalStateException("Unexpected waypoint: " + waypoint);
        };
    }

    public static Waypoint getWaypointAfterCornerOnFirstCircle(Waypoint waypoint) {
        return switch (waypoint) {
            case CORNER_SW_4 -> Waypoint.CORNER_NW_4;
            case CORNER_NW_4 -> Waypoint.CORNER_NE_4;
            case CORNER_NE_4 -> Waypoint.CORNER_SE_4;
            case CORNER_SE_4 -> Waypoint.CORNER_SW_4;

            case CORNER_SW_3 -> Waypoint.CORNER_NW_3;
            case CORNER_NW_3 -> Waypoint.CORNER_NE_3;
            case CORNER_NE_3 -> Waypoint.CORNER_SE_3;
            case CORNER_SE_3 -> Waypoint.CORNER_SW_3;

            case CORNER_SW_2 -> Waypoint.CORNER_NW_2;
            case CORNER_NW_2 -> Waypoint.CORNER_NE_2;
            case CORNER_NE_2 -> Waypoint.CORNER_SE_2;
            case CORNER_SE_2 -> Waypoint.CORNER_SW_2;

            case CORNER_SW_1 -> Waypoint.CHECKPOINT_WEST_0;
            case CORNER_NW_1 -> Waypoint.CORNER_NE_1;
            case CORNER_NE_1 -> Waypoint.CHECKPOINT_EAST_1;
            case CORNER_SE_1 -> Waypoint.CORNER_SW_1;
            default -> throw new IllegalStateException("Unexpected waypoint: " + waypoint);
        };
    }
}
