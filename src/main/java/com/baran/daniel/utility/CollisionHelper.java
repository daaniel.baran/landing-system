package com.baran.daniel.utility;

import com.baran.daniel.config.PropertyLoader;
import com.baran.daniel.dto.PlaneDto;
import com.baran.daniel.model.CollisionAvoidance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CollisionHelper {

    private static final Logger LOG = LoggerFactory.getLogger(CollisionHelper.class);

    private static final class Constants {
        private static final double MAX_HEADING_ADJUSTMENT = PropertyLoader.getMaxTurnRate();
        private static final int MAX_VELOCITY_ADJUSTMENT = PropertyLoader.getMaxVelocityAdjustment();
    }

    private CollisionHelper() {
    }

    public enum CollisionDirection {
        RIGHT, LEFT, FRONT, BACK
    }

    public static double calculateRelativeBearingInDegrees(PlaneDto planeOne, PlaneDto planeTwo) {
        double absoluteBearingInRadians = DynamicsHelper.calculateRequiredHeadingInRadians(planeOne.getCoordinates(), planeTwo.getCoordinates());
        double absoluteBearingInDegrees = Math.toDegrees(absoluteBearingInRadians - planeOne.getDynamics().getCurrentHeading());
        return adjustBearing(absoluteBearingInDegrees);
    }

    public static CollisionDirection getCollisionDirection(double bearing) {
        if ((bearing >= 315 && bearing <= 360) || (bearing >= 0 && bearing <= 45))
            return CollisionDirection.FRONT;
        else if (bearing > 45 && bearing < 135)
            return CollisionDirection.RIGHT;
        else if (bearing >= 135 && bearing < 225)
            return CollisionDirection.BACK;
        else if (bearing >= 225 && bearing < 315)
            return CollisionDirection.LEFT;
        else
            throw new IllegalArgumentException("Invalid bearing: " + bearing);
    }

    public static CollisionAvoidance getCollisionAvoidance(long id, CollisionDirection collisionDirection) {
        return switch (collisionDirection) {
            case FRONT -> {
                LOG.info("Plane {} adjusted by velocity reduction", id);
                yield new CollisionAvoidance(
                        null,
                        null,
                        -Constants.MAX_VELOCITY_ADJUSTMENT);
            }
            case BACK -> {
                LOG.info("Plane {} adjusted by velocity increase", id);
                yield new CollisionAvoidance(
                        null,
                        null,
                        Constants.MAX_VELOCITY_ADJUSTMENT);
            }
            case LEFT -> {
                LOG.info("Plane {} adjusted by heading increase, and velocity increase", id);
                yield new CollisionAvoidance(
                        Constants.MAX_HEADING_ADJUSTMENT,
                        null,
                        Constants.MAX_VELOCITY_ADJUSTMENT);
            }
            case RIGHT -> {
                LOG.info("Plane {} adjusted by heading decrease", id);
                yield new CollisionAvoidance(
                        -Constants.MAX_HEADING_ADJUSTMENT,
                        null,
                        Constants.MAX_VELOCITY_ADJUSTMENT);
            }
        };
    }

    private static double adjustBearing(double bearing) {
        if (bearing < 0)
            bearing += 360;
        return bearing;
    }
}
