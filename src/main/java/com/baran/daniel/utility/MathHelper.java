package com.baran.daniel.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public final class MathHelper {

    private MathHelper() {
    }

    private static Random random;
    private static final Logger logger = LoggerFactory.getLogger(MathHelper.class);

    static {
        try {
            random = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            logger.warn("Random instance generate failed!");
        }
    }

    public static int getRandomInRange(int min, int max) {
        return random.nextInt(max - min) + min;
    }

    public static int getRandomFromArray(int[] array) {
        return array[random.nextInt(array.length)];
    }

    public static int alignValue(int value, int max) {
        if (value < 0) return 0;
        return Math.min(value, max);
    }
}
