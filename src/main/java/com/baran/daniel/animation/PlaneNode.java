package com.baran.daniel.animation;

import com.baran.daniel.model.Coordinates;
import com.baran.daniel.model.FlightState.FlightStatus;
import lombok.Getter;
import lombok.Setter;
import processing.core.PApplet;
import processing.core.PVector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
public class PlaneNode {
    final List<PVector> trail = Collections.synchronizedList(new ArrayList<>());
    private long id;
    PVector position;
    FlightStatus status;

    public PlaneNode(long id, float x, float y, float z) {
        this.id = id;
        this.position = new PVector(x, -y, z);
        this.trail.add(position.copy());
        this.status = FlightStatus.NEW;
    }

    void update(Coordinates coordinates, FlightStatus status) {
        position.set((float) coordinates.getX(), -(float) coordinates.getY(), (float) coordinates.getZ());
        trail.add(position.copy());
        this.status = status;
    }

    void drawPositionLine(PApplet applet) {
        float x = position.x;
        float y = -position.y;
        float z = position.z;

        applet.stroke(200, 200, 200);
        applet.strokeWeight(0.25F);
        applet.line(x, y, 0, x, y, z);
    }

    public void drawTrail(PApplet applet) {
        applet.noFill();
        applet.stroke(200, 200, 200);
        applet.strokeWeight(0.25F);
        applet.beginShape();
        synchronized (trail) {
            for (PVector p : trail) {
                applet.vertex(p.x, -p.y, p.z);
            }
            applet.endShape();
        }
    }
}
