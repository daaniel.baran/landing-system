package com.baran.daniel.animation;

import com.baran.daniel.dto.PlaneDto;
import com.baran.daniel.model.Coordinates;
import com.baran.daniel.model.FlightState;
import com.baran.daniel.observer.PlaneObserver;
import com.baran.daniel.service.Plane;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import processing.core.PApplet;
import processing.core.PVector;
import processing.event.MouseEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Setter
@Getter
public class Animation extends PApplet implements PlaneObserver {

    private static final float CAMERA_INITIAL_X = 15000;
    private static final float CAMERA_INITIAL_Y = 5000;
    private static final float CAMERA_INITIAL_Z = 10000;
    private static final float PAN_SPEED = 2f;
    private static final float ZOOM_SPEED = 0.01f;
    private static final float NEAR = 10;
    private static final float FAR = 40000;

    private PVector cameraPosition = new PVector(CAMERA_INITIAL_X, CAMERA_INITIAL_Y, CAMERA_INITIAL_Z);
    private PVector prevMousePos = new PVector(0, 0, 0);
    private PVector toOrigin = new PVector(0, 0, 0).sub(cameraPosition);
    private float fov = (float) (PI / 4.0);
    private float lastMouseX;
    private float lastMouseY;
    private float deltaX = 0;
    private float deltaY = 0;
    private final DrawHelper drawHelper;
    private List<PlaneNode> planeNodes;
    private List<Plane> planes;

    public Animation(List<Plane> planesToRun) {
        this.planes = planesToRun;
        this.drawHelper = new DrawHelper(this);
        this.planeNodes = mapPlanesToPlaneNodes(planesToRun);
        this.planes.forEach(plane -> plane.setObserver(this));
    }

    private List<PlaneNode> mapPlanesToPlaneNodes(List<Plane> planesToRun) {
        return planesToRun
                .stream()
                .map(plane -> {
                    Coordinates xyz = plane.getState().getCoordinates();
                    return new PlaneNode(plane.getId(), (float) xyz.getX(), -(float) xyz.getY(), (float) xyz.getZ());
                }).collect(Collectors.toCollection(ArrayList<PlaneNode>::new));
    }

    @Override
    public void settings() {
        size(1400, 800, P3D);
        toOrigin.normalize();
    }

    @Override
    public void setup() {
        smooth();
        noStroke();
        float aspectRatio = (float) width / height;
        perspective(fov, aspectRatio, NEAR, FAR);
    }

    @Override
    public void draw() {
        background(51);
        cameraSetup();
        drawHelper.drawStaticElements();
        drawPlanes();
        prevMousePos.set(mouseX, mouseY, 0);
    }

    @Override
    public void mouseWheel(MouseEvent event) {
        float e = event.getCount();
        fov += e * ZOOM_SPEED;
        fov = constrain(fov, (float) (PI / 8.0), (float) (3 * PI / 4.0));
        float aspectRatio = (float) width / height;
        perspective(fov, aspectRatio, NEAR, FAR);
    }

    @Override
    public void mousePressed() {
        lastMouseX = mouseX;
        lastMouseY = mouseY;
    }

    @Override
    public void mouseDragged() {
        float currentDeltaX = lastMouseX - mouseX;
        float currentDeltaY = lastMouseY - mouseY;

        deltaX += currentDeltaX * PAN_SPEED;
        deltaY += currentDeltaY * PAN_SPEED;

        lastMouseX = mouseX;
        lastMouseY = mouseY;
    }

    private void drawPlanes() {
        pushMatrix();
        planeNodes.stream()
                .filter(this::isPlaneFlying)
                .forEach(node -> {
                    stroke(color(255, 0, 0, 200));
                    strokeWeight(5);
                    point(node.position.x, -node.position.y, node.position.z);
                    node.drawPositionLine(this);
                    node.drawTrail(this);
                });
        popMatrix();
    }

    private void cameraSetup() {
        beginCamera();
        camera(cameraPosition.x + deltaX, cameraPosition.y + deltaY, cameraPosition.z, 5000, -5000, 2500, 0, 0, -1);
        endCamera();
    }

    @Override
    public void updatePlane(PlaneDto plane) {
        findPlaneNode(plane).ifPresent(node -> {
            if (hasFlightEnded(node)) {
                removePlane(plane);
                planeNodes.remove(node);
            } else {
                Coordinates coordinates = mapCoordinatesForPlaneNode(plane);
                node.update(coordinates, plane.getStatus());
            }
        });
    }

    @Override
    public void removePlane(PlaneDto plane) {
        findPlaneNode(plane)
                .ifPresent(planeNode -> planeNodes.remove(planeNode));
    }

    private Coordinates mapCoordinatesForPlaneNode(PlaneDto plane) {
        return new Coordinates(
                plane.getCoordinates().getX(),
                -plane.getCoordinates().getY(),
                plane.getCoordinates().getZ());
    }

    private Optional<PlaneNode> findPlaneNode(PlaneDto plane) {
        return planeNodes
                .stream()
                .filter(n -> n.getId() == plane.getId())
                .findFirst();
    }

    private boolean isPlaneFlying(PlaneNode node) {
        return node.status.equals(FlightState.FlightStatus.FLYING);
    }

    private boolean hasFlightEnded(PlaneNode node) {
        return node.status.equals(FlightState.FlightStatus.ENDED) || node.status.equals(FlightState.FlightStatus.CRASHED);
    }
}