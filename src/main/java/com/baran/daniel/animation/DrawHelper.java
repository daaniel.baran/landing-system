package com.baran.daniel.animation;

import com.baran.daniel.model.Waypoint;
import processing.core.PApplet;

import static java.lang.Math.PI;

public class DrawHelper {

    private final PApplet applet;

    public DrawHelper(PApplet applet) {
        this.applet = applet;
    }

    public void drawStaticElements() {
        drawCube();
        drawCoordinateSystem();
        drawRunways();
        drawLabels();
        drawGrid();
    }

    private void drawCube() {
        applet.pushMatrix();
        applet.translate(5000, -5000, 2500);
        applet.noFill();
        applet.box(10000, 10000, 5000);
        applet.stroke(255, 255, 255);
        applet.popMatrix();
    }

    private void drawCoordinateSystem() {
        applet.strokeWeight(4);
        applet.stroke(255, 0, 0);
        applet.line(0, -10000, 0, 10000, -10000, 0);

        applet.strokeWeight(4);
        applet.stroke(0, 255, 0);
        applet.line(0, 0, 0, 0, -10000, 0);

        applet.strokeWeight(4);
        applet.stroke(0, 0, 255);
        applet.line(0, -10000, 0, 0, -10000, 5000);
        applet.strokeWeight(1);
    }


    private void drawRunway(int x, int y) {
        applet.stroke(0);
        applet.fill(165, 151, 145);
        applet.rect(x, y, 8000, 500);
    }

    private void drawRunways() {
        drawRunway(1000, -4750);
        drawRunway(1000, -5750);
    }

    private void drawLabels() {
        applet.textSize(1500);
        applet.fill(255, 0, 0);
        applet.text("X", 5000, -10500, 0);

        applet.fill(0, 255, 0);
        applet.text("Y", -1000, -5000, 0);

        applet.pushMatrix();
        applet.rotateX((float) (-PI / 2));
        applet.fill(0, 0, 255);
        applet.text("Z", 200, -2500, -10000);
        applet.popMatrix();
    }

    private void drawGrid() {
        applet.pushStyle();
        applet.stroke(150);
        for (int y = 0; y >= -10000; y -= 1000) {
            applet.line(0, y, 0, 10000, y, 0);
        }

        for (int x = 0; x <= 10000; x += 1000) {
            applet.line(x, 0, 0, x, -10000, 0);
        }
        applet.popStyle();
    }

    private void drawWaypoints() {
        Waypoint[] waypoints = Waypoint.values();
        for (Waypoint waypoint : waypoints) {
            applet.noStroke();
            applet.pushMatrix();
            applet.translate((float) waypoint.getCoordinates().getX(), -(float) waypoint.getCoordinates().getY(), (float) waypoint.getCoordinates().getZ());
            applet.fill(165, 151, 145);
            applet.sphere(50);
            applet.popMatrix();
        }
    }

}