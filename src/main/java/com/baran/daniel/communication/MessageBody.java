package com.baran.daniel.communication;

import com.baran.daniel.dto.PlaneDto;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class MessageBody {

    private final PlaneDto planeDTO;
    private String errorMessage;
}
