package com.baran.daniel.communication;

import com.baran.daniel.dto.PlaneDto;
import lombok.Getter;
import lombok.Setter;

import static com.baran.daniel.communication.Message.MessageType.*;

@Getter
@Setter
public final class Message {

    private final MessageType messageType;
    private final MessageBody body;

    private Message(MessageType messageType, MessageBody body) {
        this.messageType = messageType;
        this.body = body;
    }

    public enum MessageType {
        ERROR, PERMISSION_TO_LAND, NEW_PLANE, LANDED, CRASHED, POSITION_UPDATE, POSSIBLE_COLLISION, LOW_FUEL
    }

    public static Message errorMessage(String message) {
        return new Message(ERROR, MessageBody
                .builder()
                .errorMessage(message)
                .build());
    }

    public static Message requestToJoin(PlaneDto plane) {
        return new Message(NEW_PLANE, MessageBody
                .builder()
                .planeDTO(plane)
                .build());
    }

    public static Message requestToLand(PlaneDto plane) {
        return new Message(PERMISSION_TO_LAND, MessageBody.builder()
                .planeDTO(plane)
                .build());
    }

    public static Message positionUpdate(PlaneDto plane) {
        return new Message(POSITION_UPDATE, MessageBody
                .builder()
                .planeDTO(plane)
                .build()
        );
    }

    public static Message successfullyLanding(PlaneDto plane) {
        return new Message(LANDED, MessageBody
                .builder()
                .planeDTO(plane)
                .build());
    }

    public static Message possibleCollision(PlaneDto plane) {
        return new Message(POSSIBLE_COLLISION, MessageBody
                .builder()
                .planeDTO(plane)
                .build());
    }

    public static Message lowFuelLevel(PlaneDto plane) {
        return new Message(LOW_FUEL, MessageBody
                .builder()
                .planeDTO(plane)
                .build());
    }
}
