package com.baran.daniel.model;

import com.baran.daniel.utility.CoordinatesHelper;
import com.baran.daniel.utility.MathHelper;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Coordinates {

    private double x;
    private double y;
    private double z;

    public Coordinates(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Coordinates(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Coordinates() {
        Coordinates randomCoordinates = getRandomCoordinates();
        this.x = randomCoordinates.getX();
        this.y = randomCoordinates.getY();
        this.z = randomCoordinates.getZ();
    }

    public Coordinates(Coordinates coordinates) {
        this.x = coordinates.getX();
        this.y = coordinates.getY();
        this.z = coordinates.getZ();
    }

    public void updateCoordinatesByHeading(double velocity, double currentHeading, double altitudeChange) {
        this.x = CoordinatesHelper.calculateXCoordinateByHeading(x, currentHeading, velocity);
        this.y = CoordinatesHelper.calculateYCoordinateByHeading(y, currentHeading, velocity);
        this.z -= altitudeChange;
    }

    private Coordinates getRandomCoordinates() {
        int randomAxisXorY = MathHelper.getRandomFromArray(new int[]{0, 1}); //random axis
        int beginningOrEndOfAxis = MathHelper.getRandomFromArray(new int[]{0, 9999}); //random start or end of axis
        int randomValue = MathHelper.getRandomInRange(0, 9999);

        int randomX = randomAxisXorY == 0 ? randomValue : beginningOrEndOfAxis;
        int randomY = randomAxisXorY == 1 ? randomValue : beginningOrEndOfAxis;
        int randomZ = MathHelper.getRandomInRange(4600, 4999);

        return new Coordinates(randomX, randomY, randomZ);
    }

    public Coordinates projectNextCoordinates(FlightDynamics dynamics, int secondsAhead) {
        int distance = dynamics.getVelocity() * secondsAhead;
        double deltaX = distance * Math.sin(Math.toRadians(dynamics.getCurrentHeading()));
        double deltaY = distance * Math.cos(Math.toRadians(dynamics.getCurrentHeading()));
        double deltaZ = dynamics.getAltitudeChange();

        return new Coordinates(x + deltaX, y + deltaY, z + deltaZ);
    }

    @Override
    public String toString() {
        return String.format("%.2f - %.2f - %.2f", x, y, z);
    }
}
