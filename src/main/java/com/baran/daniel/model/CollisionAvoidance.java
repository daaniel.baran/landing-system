package com.baran.daniel.model;

public record CollisionAvoidance(
        Double adjustedHeading,
        Double adjustedAltitude,
        Integer adjustedVelocity) { }
