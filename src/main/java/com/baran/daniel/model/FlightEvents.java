package com.baran.daniel.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
public class FlightEvents {

    private boolean canJoin;
    private int currentStep;
    private CollisionAvoidance collisionAvoidance;
    private LandingDetails landingDetails;

    public FlightEvents() {
        this.canJoin = false;
        this.collisionAvoidance = null;
        this.landingDetails = new LandingDetails();
    }
}
