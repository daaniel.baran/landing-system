package com.baran.daniel.model;

import com.baran.daniel.config.PropertyLoader;
import com.baran.daniel.utility.CoordinatesHelper;
import com.baran.daniel.utility.DynamicsHelper;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlightDynamics {

    private static class Constants {
        private static final int MIN_CRUISING_ALTITUDE = PropertyLoader.getMinCruisingAltitude();
        private static final int CRUISING_VELOCITY = PropertyLoader.getCruisingVelocity();
        private static final int LANDING_VELOCITY = PropertyLoader.getLandingVelocity();
    }

    private int velocity;
    private double altitudeChange;
    private long fuel;
    private double currentHeading;

    public FlightDynamics() {
        this.velocity = PropertyLoader.getCruisingVelocity();
        this.altitudeChange = 0;
        this.fuel = PropertyLoader.getMaxFuel();
    }

    public FlightDynamics(int velocity, double altitudeChange, long fuel, double currentHeading) {
        this.velocity = velocity;
        this.altitudeChange = altitudeChange;
        this.fuel = fuel;
        this.currentHeading = currentHeading;
    }

    public void consumeFuel() {
        this.fuel -= 1;
    }

    public boolean isOutOfFuel() {
        return this.fuel <= 0;
    }

    public void updateHeadingByWaypoint(Coordinates coordinates, Waypoint waypoint) {
        double requiredHeading = DynamicsHelper.calculateRequiredHeadingInRadians(coordinates, waypoint.getCoordinates());
        double headingDifference = requiredHeading - this.currentHeading;

        if (this.currentHeading == 0)
            this.currentHeading = headingDifference;
        headingDifference = DynamicsHelper.normalizeAngle(headingDifference);

        double turn = DynamicsHelper.calculateMaxTurn(headingDifference);
        this.currentHeading = DynamicsHelper.normalizeAngle(currentHeading + turn);
    }

    public void updateHeadingForCollisionAvoidance(Double adjustedHeading) {
        this.currentHeading += adjustedHeading;
    }

    public boolean isLowFuelLevel() {
        return this.fuel <= 1800;
    }


    public void updateHeightChangeForCollisionAvoidance(Double adjustedAltitude) {
        this.altitudeChange += adjustedAltitude;
    }

    public void updateHeightChange(Coordinates coordinates, Waypoint waypoint) {
        double planeAltitude = coordinates.getZ();
        double waypointAltitude = waypoint.getCoordinates().getZ();
        double altitudeDifference = planeAltitude - waypointAltitude;

        if (altitudeDifference <= 0) {
            this.altitudeChange = 0;
            return;
        }

        double horizontalDistanceToWaypoint = CoordinatesHelper.distanceBetweenTwoCoordinates2D(coordinates, waypoint.getCoordinates());
        double timeToWaypoint = horizontalDistanceToWaypoint / velocity;

        if (timeToWaypoint == 0) {
            this.altitudeChange = 0;
            return;
        }
        double requiredDescentRate = altitudeDifference / timeToWaypoint;

        if (waypoint.isCheckpoint()) {
            double projectedAltitude = planeAltitude - requiredDescentRate;
            if (projectedAltitude < Constants.MIN_CRUISING_ALTITUDE) {
                requiredDescentRate = (planeAltitude - Constants.MIN_CRUISING_ALTITUDE) / timeToWaypoint;
            }
        }
        this.altitudeChange = requiredDescentRate;
    }

    public void updateVelocityForCollisionAvoidance(Integer collisionAvoidance) {
        this.velocity += collisionAvoidance;
    }

    public void updateVelocityBasedOnWaypoint(Waypoint waypoint) {
        if (waypoint.isCheckpoint() || waypoint.isCorner())
            this.velocity = Constants.CRUISING_VELOCITY;
        else
            this.velocity = Constants.LANDING_VELOCITY;
    }
}
