package com.baran.daniel.model;

import lombok.Getter;
import lombok.Setter;

import java.util.concurrent.Semaphore;

@Setter
@Getter
public class Corridor {

    private int id;
    private boolean isBusy;
    private Long planeId;
    private Semaphore semaphore;

    public Corridor(int id) {
        this.semaphore = new Semaphore(1);
        this.id = id;
        this.isBusy = false;
    }

    public void setCorridorAsBusy(long planeId) {
        boolean acquired = this.semaphore.tryAcquire();

        if (acquired) {
            this.planeId = planeId;
            this.isBusy = true;
        }
    }

    public void releaseCorridor() {
        this.semaphore.release();
        this.planeId = null;
        this.isBusy = false;
    }
}