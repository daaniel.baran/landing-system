package com.baran.daniel.model;

import com.baran.daniel.utility.FlightPlanHelper;
import lombok.Getter;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Getter
public class FlightPlan {

    private Waypoint currentWaypoint;
    private final Set<Waypoint> visitedWaypoints;

    public FlightPlan(Coordinates coordinates) {
        this.currentWaypoint = FlightPlanHelper.getStartingWaypoint(coordinates);
        this.visitedWaypoints = new HashSet<>();
    }

    public void updateCurrentWaypoint(FlightEvents flightEvents) {
        this.currentWaypoint = this.getNextWaypoint(flightEvents).orElse(null);
    }

    public Optional<Waypoint> getNextWaypoint(FlightEvents events) {
        boolean hasPermission = events.getLandingDetails().isHasPermission();
        boolean isSecondCircle = this.visitedWaypoints.contains(currentWaypoint);

        Waypoint nextWaypoint = null;
        if (currentWaypoint.isCorner()) {
            nextWaypoint = isSecondCircle ? FlightPlanHelper.getWaypointAfterCornerOnSecondCircle(currentWaypoint)
                    : FlightPlanHelper.getWaypointAfterCornerOnFirstCircle(currentWaypoint);
        } else if (currentWaypoint.isCheckpoint()) {
            nextWaypoint = FlightPlanHelper.getWaypointAfterCheckpoint(currentWaypoint, hasPermission);
        }
        this.visitedWaypoints.add(currentWaypoint);
        return Optional.ofNullable(nextWaypoint);
    }
}
