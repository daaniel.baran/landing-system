package com.baran.daniel.model;

import lombok.Getter;

import static com.baran.daniel.model.Waypoint.WaypointLevel.*;
import static com.baran.daniel.model.Waypoint.WaypointType.*;

@Getter
public enum Waypoint {

    CORNER_SW_4(new Coordinates(1_000, 1_000, LEVEL_4.altitude), CORNER),
    CORNER_SW_3(new Coordinates(1_000, 1_000, LEVEL_3.altitude), CORNER),
    CORNER_SW_2(new Coordinates(1_000, 1_000, LEVEL_2.altitude), CORNER),
    CORNER_SW_1(new Coordinates(1_000, 1_000, LEVEL_1.altitude), CORNER),
    CHECKPOINT_WEST_0(new Coordinates(1_500, 4_500, LEVEL_0.altitude), CHECKPOINT),
    CHECKPOINT_WEST_1(new Coordinates(1_500, 5_500, LEVEL_0.altitude), CHECKPOINT),
    CORNER_NW_4(new Coordinates(1_000, 9_000, LEVEL_4.altitude), CORNER),
    CORNER_NW_3(new Coordinates(1_000, 9_000, LEVEL_3.altitude), CORNER),
    CORNER_NW_2(new Coordinates(1_000, 9_000, LEVEL_2.altitude), CORNER),
    CORNER_NW_1(new Coordinates(1_000, 9_000, LEVEL_1.altitude), CORNER),
    CORNER_NE_4(new Coordinates(9_000, 9_000, LEVEL_4.altitude), CORNER),
    CORNER_NE_3(new Coordinates(9_000, 9_000, LEVEL_3.altitude), CORNER),
    CORNER_NE_2(new Coordinates(9_000, 9_000, LEVEL_2.altitude), CORNER),
    CORNER_NE_1(new Coordinates(9_000, 9_000, LEVEL_1.altitude), CORNER),
    CHECKPOINT_EAST_1(new Coordinates(8_500, 5_500, LEVEL_0.altitude), CHECKPOINT),
    CHECKPOINT_EAST_0(new Coordinates(8_500, 4_500, LEVEL_0.altitude), CHECKPOINT),
    CORNER_SE_4(new Coordinates(9_000, 1_000, LEVEL_4.altitude), CORNER),
    CORNER_SE_3(new Coordinates(9_000, 1_000, LEVEL_3.altitude), CORNER),
    CORNER_SE_2(new Coordinates(9_000, 1_000, LEVEL_2.altitude), CORNER),
    CORNER_SE_1(new Coordinates(9_000, 1_000, LEVEL_1.altitude), CORNER),
    RUNWAY_START_0(new Coordinates(1_500, 4_500, GROUND_LEVEL.altitude), RUNWAY),
    RUNWAY_END_0(new Coordinates(8_500, 4_500, GROUND_LEVEL.altitude), RUNWAY),
    RUNWAY_START_1(new Coordinates(1_500, 5_500, GROUND_LEVEL.altitude), RUNWAY),
    RUNWAY_END_1(new Coordinates(8_500, 5_500, GROUND_LEVEL.altitude), RUNWAY);

    private final Coordinates coordinates;
    private final WaypointType type;

    Waypoint(Coordinates coordinates, WaypointType type) {
        this.coordinates = coordinates;
        this.type = type;
    }

    public enum WaypointType {
        CHECKPOINT, CORNER, RUNWAY
    }

    public enum WaypointLevel {
        LEVEL_4(4600),
        LEVEL_3(4100),
        LEVEL_2(3600),
        LEVEL_1(3100),
        LEVEL_0(3000),
        GROUND_LEVEL(0);

        private final int altitude;

        WaypointLevel(int altitude) {
            this.altitude = altitude;
        }
    }

    public boolean isCheckpoint() {
        return this.getType().equals(CHECKPOINT);
    }

    public boolean isRunway() {
        return this.getType().equals(RUNWAY);
    }

    public boolean isCorner() {
        return this.getType().equals(CORNER);
    }
}
