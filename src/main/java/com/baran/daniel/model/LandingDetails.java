package com.baran.daniel.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LandingDetails {

    private int approaches;
    private boolean hasPermission;
    private int corridorId;

    public LandingDetails(int corridorId) {
        this.approaches = 0;
        this.hasPermission = false;
        this.corridorId = corridorId;
    }
}
