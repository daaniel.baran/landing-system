package com.baran.daniel.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class FlightState {

    private final LocalDateTime start;
    private LocalDateTime end;
    private FlightStatus status;
    private Coordinates coordinates;

    public FlightState() {
        this.status = FlightStatus.NEW;
        this.start = LocalDateTime.now();
        this.coordinates = new Coordinates();
    }

    public enum FlightStatus {
        NEW, FLYING, ENDED, CRASHED, REJECTED
    }
}
