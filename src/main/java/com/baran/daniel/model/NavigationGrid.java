package com.baran.daniel.model;

import com.baran.daniel.config.PropertyLoader;
import com.baran.daniel.dto.PlaneDto;
import com.baran.daniel.exception.InvalidCoordinatesException;
import com.baran.daniel.utility.CoordinatesHelper;
import com.baran.daniel.utility.MathHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class NavigationGrid {

    private static final Logger LOG = LoggerFactory.getLogger("NavigationGrid");

    private static final class Constants {
        private static final int CELL_SIZE = PropertyLoader.getAirspaceCellSize();
        private static final int AIRSPACE_DIMENSION_X = PropertyLoader.getAirspaceLength();
        private static final int AIRSPACE_DIMENSION_Y = PropertyLoader.getAirspaceWidth();
        private static final int AIRSPACE_DIMENSION_Z = PropertyLoader.getAirspaceHeight();
        private static final int CELLS_X = Constants.AIRSPACE_DIMENSION_X / Constants.CELL_SIZE;
        private static final int CELLS_Y = Constants.AIRSPACE_DIMENSION_Y / Constants.CELL_SIZE;
        private static final int CELLS_Z = Constants.AIRSPACE_DIMENSION_Z / Constants.CELL_SIZE;
    }

    private final Semaphore semaphore;
    private final Map<Long, PlaneDto> planes;
    private final List<Long>[][][] grid;
    private final Object[][][] cellLocks;

    public NavigationGrid() {
        this.semaphore = new Semaphore(1);
        planes = new ConcurrentHashMap<>();
        grid = new ArrayList[Constants.CELLS_X][Constants.CELLS_Y][Constants.CELLS_Z];
        cellLocks = new Object[Constants.CELLS_X][Constants.CELLS_Y][Constants.CELLS_Z];
        initializeGrid();
    }

    public void updatePlanePosition(PlaneDto plane) throws InvalidCoordinatesException {
        if (CoordinatesHelper.invalidCoordinates(plane.getCoordinates())) {
            throw new InvalidCoordinatesException("Coordinates are out of bounds.");
        }
        int[] newIndexes = getGridCellIndexes(plane.getCoordinates());
        if (planes.containsKey(plane.getId())) {
            int[] oldIndexes = getGridCellIndexes(planes.get(plane.getId()).getCoordinates());
            synchronized (cellLocks[oldIndexes[0]][oldIndexes[1]][oldIndexes[2]]) {
                grid[oldIndexes[0]][oldIndexes[1]][oldIndexes[2]].remove(plane.getId());
            }
        }
        synchronized (cellLocks[newIndexes[0]][newIndexes[1]][newIndexes[2]]) {
            grid[newIndexes[0]][newIndexes[1]][newIndexes[2]].add(plane.getId());
        }
        planes.put(plane.getId(), plane);
    }

    public List<PlaneDto> getSurroundingPlanes(PlaneDto plane) {
        int[] currentIndices = getGridCellIndexes(plane.getCoordinates());
        Set<Long> nearbyPlanes = new HashSet<>();

        for (int x = -1; x <= 1; x++)
            for (int y = -1; y <= 1; y++)
                for (int z = -1; z <= 1; z++) {
                    int xi = currentIndices[0] + x;
                    int yi = currentIndices[1] + y;
                    int zi = currentIndices[2] + z;

                    if (CoordinatesHelper.belongsToCell(xi, yi, zi)) {
                        synchronized (cellLocks[xi][yi][zi]) {
                            nearbyPlanes.addAll(grid[xi][yi][zi]);
                        }
                    }
                }
        return nearbyPlanes.stream()
                .map(planes::get)
                .filter(nearbyPlane -> plane.getId() != nearbyPlane.getId())
                .toList();
    }

    public void removePlane(long planeId) {
        boolean semaphoreAcquired = false;
        try {
            semaphoreAcquired = this.semaphore.tryAcquire(300, TimeUnit.MILLISECONDS);
            if (semaphoreAcquired && planes.containsKey(planeId)) {
                int[] oldCellIndexes = getGridCellIndexes(planes.get(planeId).getCoordinates());
                grid[oldCellIndexes[0]][oldCellIndexes[1]][oldCellIndexes[2]].remove(planeId);
                planes.remove(planeId);
            }
        } catch (InterruptedException e) {
            LOG.warn("Interrupted");
        } finally {
            if (semaphoreAcquired) this.semaphore.release();
        }
    }

    private int[] getGridCellIndexes(Coordinates coordinates) {
        int xIndex = Math.floorDiv((int) coordinates.getX(), Constants.CELL_SIZE);
        int yIndex = Math.floorDiv((int) coordinates.getY(), Constants.CELL_SIZE);
        int zIndex = Math.floorDiv((int) coordinates.getZ(), Constants.CELL_SIZE);

        xIndex = MathHelper.alignValue(xIndex, Constants.CELLS_X - 1);
        yIndex = MathHelper.alignValue(yIndex, Constants.CELLS_Y - 1);
        zIndex = MathHelper.alignValue(zIndex, Constants.CELLS_Z - 1);
        return new int[]{xIndex, yIndex, zIndex};
    }

    private void initializeGrid() {
        for (int i = 0; i < Constants.CELLS_X; i++)
            for (int j = 0; j < Constants.CELLS_Y; j++)
                for (int k = 0; k < Constants.CELLS_Z; k++) {
                    grid[i][j][k] = new ArrayList<>();
                    cellLocks[i][j][k] = new Object();
                }
    }
}