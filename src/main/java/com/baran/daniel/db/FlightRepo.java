package com.baran.daniel.db;

import com.baran.daniel.config.DatabaseConnection;
import com.baran.daniel.model.FlightState.FlightStatus;
import jooq.model.tables.records.CollisionRecord;
import jooq.model.tables.records.FlightRecord;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;

import static com.baran.daniel.model.FlightState.FlightStatus.*;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static jooq.model.Tables.COLLISION;
import static jooq.model.tables.Flight.FLIGHT;

public class FlightRepo {

    private static final Logger LOG = LoggerFactory.getLogger("PlaneRepo");
    private static final SQLDialect DIALECT = SQLDialect.POSTGRES;
    private static final DSLContext CONTEXT = DSL.using(DatabaseConnection.getConnection(), DIALECT);

    private static FlightRepo planesRepo;

    private FlightRepo() {
    }

    public static FlightRepo getInstance() {
        if (isNull(planesRepo))
            planesRepo = new FlightRepo();
        truncateTables();
        return planesRepo;
    }

    protected static void truncateTables() {
        CONTEXT.truncate(FLIGHT).execute();
        CONTEXT.truncate(COLLISION).execute();
        LOG.info("Table cleared!");
    }

    public void setFlightAsRejected(long flightId) {
        updateFlightStatus(flightId, REJECTED);
    }

    public void setFlightAsLanded(long flightId) {
        updateFlightStatus(flightId, ENDED);
    }

    public void setFlightAsCrashed(long flightId) {
        updateFlightStatus(flightId, CRASHED);
    }

    public void saveCollision(long plane1Id) {
        CollisionRecord collision = CONTEXT.fetchOne(COLLISION, COLLISION.PLANE_ID.eq((int) plane1Id));
        if (isNull(collision)) {
            collision = CONTEXT.newRecord(COLLISION);
            collision.setPlaneId((int) plane1Id);
            collision.store();
            setFlightAsCrashed(plane1Id);
        }
    }

    public void createPlane(long planeId) {
        FlightRecord flight = CONTEXT.fetchOne(FLIGHT, FLIGHT.ID.eq(planeId));
        if (isNull(flight)) {
            flight = CONTEXT.newRecord(FLIGHT);
            flight.setId(planeId);
            flight.setStartTime(LocalDateTime.now());
            flight.store();

            LOG.info("Plane with id: {} saved in db", planeId);
        }
    }

    protected void updateFlightStatus(long planeId, FlightStatus status) {
        FlightRecord flight = CONTEXT.fetchOne(FLIGHT, FLIGHT.ID.eq(planeId));
        if (nonNull(flight)) {
            CONTEXT
                    .update(FLIGHT)
                    .set(FLIGHT.FLIGHT_STATUS, status.name())
                    .set(FLIGHT.END_TIME, LocalDateTime.now())
                    .where(FLIGHT.ID.eq(planeId))
                    .execute();
            LOG.info("PlaneId:{} status updated in db to: {}", flight.getId(), status);
        }
    }
}
