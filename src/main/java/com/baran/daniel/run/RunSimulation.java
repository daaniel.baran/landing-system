package com.baran.daniel.run;

import com.baran.daniel.animation.Animation;
import com.baran.daniel.config.PropertyLoader;
import com.baran.daniel.service.Plane;
import lombok.extern.slf4j.Slf4j;
import processing.core.PApplet;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

@Slf4j
public class RunSimulation {

    public static final boolean TOGGLE_RUN_ANIMATION = PropertyLoader.isRunAnimationOn();
    private static final int PLANES = PropertyLoader.getPlanesForTesting();

    public static void main(String[] args) {
        try {
            testRun();
        } catch (InterruptedException e) {
            log.error("Interrupted {}", e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

    private static void testRun() throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(PropertyLoader.getMaxCapacity());
        List<Plane> planesToRun = createPlanes();

        if (TOGGLE_RUN_ANIMATION) {
            Animation animation = new Animation(planesToRun);
            String[] appletArgs = new String[]{animation.getClass().getCanonicalName()};
            PApplet.runSketch(appletArgs, animation);
        }

        for (Plane plane : planesToRun) {
            executor.execute(plane);
            Thread.sleep(15000);
        }
    }

    private static List<Plane> createPlanes() {
        return IntStream.range(0, PLANES).mapToObj(Plane::new).toList();
    }
}
