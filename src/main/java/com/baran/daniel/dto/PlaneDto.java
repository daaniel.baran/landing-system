package com.baran.daniel.dto;

import com.baran.daniel.model.Coordinates;
import com.baran.daniel.model.FlightDynamics;
import com.baran.daniel.model.FlightEvents;
import com.baran.daniel.model.FlightState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlaneDto {
    private long id;
    private Coordinates coordinates;
    private FlightState.FlightStatus status;
    private FlightDynamics dynamics;
    private FlightEvents events;

    public PlaneDto getDeepCopy() {
        return PlaneDto.builder()
                .coordinates(coordinates)
                .dynamics(dynamics)
                .events(events)
                .build();
    }
}
