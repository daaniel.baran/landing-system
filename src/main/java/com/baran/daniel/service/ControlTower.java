package com.baran.daniel.service;

import com.baran.daniel.config.PropertyLoader;
import com.baran.daniel.db.FlightRepo;
import com.baran.daniel.dto.PlaneDto;
import com.baran.daniel.exception.InvalidCoordinatesException;
import com.baran.daniel.model.NavigationGrid;
import com.baran.daniel.model.CollisionAvoidance;
import com.baran.daniel.model.Corridor;
import com.baran.daniel.service.collision.AvoidanceService;
import com.baran.daniel.service.collision.DetectionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static com.baran.daniel.model.FlightState.FlightStatus;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class ControlTower {

    private static final Logger LOG = LoggerFactory.getLogger("ControlTower");

    private final Semaphore semaphore;
    private final FlightRepo flightRepo;
    private final Map<Integer, Corridor> corridors;
    private final AtomicInteger currentCapacity;
    private final NavigationGrid navigationGrid;
    private final AvoidanceService avoidanceService;
    private final DetectionService detectionService;
    private static final int MAX_CAPACITY = PropertyLoader.getMaxCapacity();
    private static ControlTower tower;

    private ControlTower() {
        semaphore = new Semaphore(1);
        flightRepo = FlightRepo.getInstance();
        currentCapacity = new AtomicInteger(0);
        corridors = new HashMap<>();
        corridors.put(0, new Corridor(0));
        corridors.put(1, new Corridor(1));
        navigationGrid = new NavigationGrid();
        detectionService = new DetectionService(navigationGrid);
        avoidanceService = new AvoidanceService(navigationGrid, detectionService);
    }

    public static ControlTower getInstance() {
        if (isNull(tower)) {
            tower = new ControlTower();
        }
        return tower;
    }

    public boolean checkCorridorAvailability(long planeId, int corridorId) {
        Corridor corridor = corridors.get(corridorId);
        if (corridor.isBusy())
            return false;
        else {
            corridor.setCorridorAsBusy(planeId);
            return true;
        }
    }

    public void removePlaneFromMonitor(long planeId, int corridorId, FlightStatus status) {
        switch (status) {
            case ENDED -> {
                flightRepo.setFlightAsLanded(planeId);
                Corridor corridor = corridors.get(corridorId);
                corridor.releaseCorridor();
            }
            case CRASHED -> flightRepo.setFlightAsCrashed(planeId);
            case REJECTED -> flightRepo.setFlightAsRejected(planeId);
            default -> throw new IllegalStateException("Unexpected value: " + status);
        }
        LOG.info("ControlTower removed Plane: {} from the monitor", planeId);
        navigationGrid.removePlane(planeId);
    }

    public boolean handlePermissionToJoin(PlaneDto plane) {
        try {
            boolean semaphoreAcquired = this.semaphore.tryAcquire(500, TimeUnit.MILLISECONDS);
            if (semaphoreAcquired && currentCapacity.get() <= MAX_CAPACITY) {
                boolean possibleCollision = detectionService.hasPotentialCollision(plane);
                if (!possibleCollision) {
                    flightRepo.createPlane(plane.getId());
                    navigationGrid.updatePlanePosition(plane);
                    currentCapacity.getAndIncrement();
                    return true;
                }
            }
        } catch (InterruptedException e) {
            LOG.error("Sorry, permission wasn't granted properly. Timeout exceeded!");
            Thread.currentThread().interrupt();
        } catch (InvalidCoordinatesException e) {
            LOG.error("Sorry, plane's coordinates: {} are invalid", plane.getCoordinates());
        } finally {
            this.semaphore.release();
        }
        return false;
    }

    public void updatePosition(PlaneDto plane) {
        try {
            boolean semaphoreAcquired = semaphore.tryAcquire(200, TimeUnit.MILLISECONDS);
            if (semaphoreAcquired)
                this.navigationGrid.updatePlanePosition(plane);
        } catch (InterruptedException e) {
            LOG.error("Sorry, position wasn't updated properly. Timeout exceeded!");
            Thread.currentThread().interrupt();
        } catch (InvalidCoordinatesException e) {
            LOG.error("Sorry, plane's coordinates were invalid");
        } finally {
            this.semaphore.release();
        }
    }

    public Optional<CollisionAvoidance> checkForCollision(PlaneDto plane) {
        try {
            boolean semaphoreAcquired = this.semaphore.tryAcquire(200, TimeUnit.MILLISECONDS);
            if (semaphoreAcquired) {
                boolean possibleCollision = detectionService.hasPotentialCollision(plane);
                if (possibleCollision) {
                    CollisionAvoidance collisionAvoidance = avoidanceService.avoidCollision(plane);
                    if (nonNull(collisionAvoidance))
                        return Optional.of(collisionAvoidance);
                }
            }
        } catch (InterruptedException e) {
            LOG.error("Sorry, position wasn't updated properly. Timeout exceeded!");
            Thread.currentThread().interrupt();
        } finally {
            this.semaphore.release();
        }
        return Optional.empty();
    }

    private void handleCollision(PlaneDto plane) {
        //TODO
        navigationGrid.removePlane(plane.getId());
        flightRepo.saveCollision(plane.getId());
    }
}