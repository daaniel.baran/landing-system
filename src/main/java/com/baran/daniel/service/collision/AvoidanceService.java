package com.baran.daniel.service.collision;

import com.baran.daniel.dto.PlaneDto;
import com.baran.daniel.model.CollisionAvoidance;
import com.baran.daniel.model.NavigationGrid;
import com.baran.daniel.utility.CollisionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class AvoidanceService {

    private static final Logger LOG = LoggerFactory.getLogger("AvoidanceService");
    private final NavigationGrid navigationGrid;
    private final DetectionService detectionService;
    private final CollisionTracker collisionTracker;

    public AvoidanceService(NavigationGrid navigationGrid, DetectionService detectionService) {
        this.navigationGrid = navigationGrid;
        this.detectionService = detectionService;
        this.collisionTracker = CollisionTracker.getInstance();
    }

    public CollisionAvoidance avoidCollision(PlaneDto plane) {
        List<PlaneDto> nearbyPlanes = navigationGrid.getSurroundingPlanes(plane);

        for (PlaneDto nearbyPlane : nearbyPlanes) {
            if (detectionService.hasPossibleCollisionInNextMoves(plane, nearbyPlane, 3) && !collisionTracker.collisionExists(plane.getId(), nearbyPlane.getId())) {
                collisionTracker.addCollision(plane.getId(), nearbyPlane.getId());
                return resolveCollision(plane, nearbyPlane);
            }
        }
        return null;
    }

    private CollisionAvoidance resolveCollision(PlaneDto plane1, PlaneDto plane2) {
        double bearing = CollisionHelper.calculateRelativeBearingInDegrees(plane1, plane2);
        CollisionHelper.CollisionDirection collisionDirection = CollisionHelper.getCollisionDirection(bearing);

        LOG.info("Plane {} is from {} to Plane {}", plane2.getId(), collisionDirection, plane1.getId());
        collisionTracker.resolveCollision(plane1.getId(), plane2.getId());
        return CollisionHelper.getCollisionAvoidance(plane1.getId(), collisionDirection);
    }
}
