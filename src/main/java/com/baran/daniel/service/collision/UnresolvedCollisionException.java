package com.baran.daniel.service.collision;

public class UnresolvedCollisionException extends Exception {
    public UnresolvedCollisionException(String s) {
        super(s);
    }
}
