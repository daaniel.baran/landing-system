package com.baran.daniel.service.collision;

import com.baran.daniel.config.PropertyLoader;
import com.baran.daniel.dto.PlaneDto;
import com.baran.daniel.model.Coordinates;
import com.baran.daniel.model.NavigationGrid;
import com.baran.daniel.utility.CoordinatesHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class DetectionService {

    private static final Logger LOG = LoggerFactory.getLogger("DetectionService");
    private static final int MIN_SAFE_DISTANCE = PropertyLoader.getMinSafeDistance();

    private final NavigationGrid navigationGrid;

    public DetectionService(NavigationGrid navigationGrid) {
        this.navigationGrid = navigationGrid;
    }

    public boolean hasPotentialCollision(PlaneDto currentPlane) {
        List<PlaneDto> nearbyPlanes = navigationGrid.getSurroundingPlanes(currentPlane);
        return nearbyPlanes
                .stream()
                .anyMatch(nextPlane -> arePlanesTooClose(currentPlane, nextPlane));
    }

    public boolean hasPossibleCollisionInNextMoves(PlaneDto plane1, PlaneDto plane2, int secondsAhead) {
        for (int i = 0; i < secondsAhead; i++) {
            Coordinates projectedPosition1 = plane1.getCoordinates().projectNextCoordinates(plane1.getDynamics(), i);
            Coordinates projectedPosition2 = plane2.getCoordinates().projectNextCoordinates(plane2.getDynamics(), i);

            if (isDistanceDangerous(projectedPosition1, projectedPosition2))
                return true;
        }
        return false;
    }

    private boolean arePlanesTooClose(PlaneDto currentPlane, PlaneDto nearbyPlane) {
        boolean isTooClose = isDistanceDangerous(currentPlane.getCoordinates(), nearbyPlane.getCoordinates());
        if (isTooClose) {
            LOG.warn("!!!---POSSIBLE COLLISION---!!! between: {}, and {}", currentPlane.getId(), nearbyPlane.getId());
            return true;
        }
        return false;
    }

    private boolean isDistanceDangerous(Coordinates projectedPosition1, Coordinates projectedPosition2) {
        return CoordinatesHelper.distanceBetweenTwoCoordinates3D(projectedPosition1, projectedPosition2) < MIN_SAFE_DISTANCE;
    }
}
