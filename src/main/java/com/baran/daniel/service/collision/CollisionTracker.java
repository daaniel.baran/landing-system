package com.baran.daniel.service.collision;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Objects.isNull;

public class CollisionTracker {

    private static CollisionTracker collisionTracker;

    public static CollisionTracker getInstance() {
        if (isNull(collisionTracker))
            collisionTracker = new CollisionTracker();
        return collisionTracker;
    }

    private CollisionTracker() {
    }

    private final Set<Pair> possibleCollisions = ConcurrentHashMap.newKeySet();

    public void addCollision(long plane1, long plane2) {
        Pair collisionPair = new Pair(plane1, plane2);
        possibleCollisions.add(collisionPair);
    }

    public void resolveCollision(long plane1, long plane2) {
        Pair collisionPair = new Pair(plane1, plane2);
        possibleCollisions.remove(collisionPair);
    }

    public boolean collisionExists(long plane1, long plane2) {
        return possibleCollisions.contains(new Pair(plane1, plane2));
    }

    record Pair(Long first, Long second) {
    }
}
