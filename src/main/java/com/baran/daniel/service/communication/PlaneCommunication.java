package com.baran.daniel.service.communication;

import com.baran.daniel.communication.Message;
import com.baran.daniel.dto.PlaneDto;
import com.baran.daniel.model.Coordinates;
import com.baran.daniel.model.FlightDynamics;
import com.baran.daniel.model.FlightEvents;
import com.baran.daniel.model.FlightState;
import lombok.Getter;

@Getter
public class PlaneCommunication {

    private final Communication communication;
    private final MessageService messageService;

    public PlaneCommunication() {
        this.communication = new Communication();
        this.messageService = communication.getMessageService();
    }

    public boolean requestPermissionToJoin(long id, FlightEvents events, Coordinates coordinates, FlightState.FlightStatus status) {
        PlaneDto plane = PlaneDto.builder()
                .id(id)
                .events(events)
                .status(status)
                .coordinates(coordinates)
                .build();
        Message request = Message.requestToJoin(plane);
        messageService.send(request);

        Message response = messageService.getMessage();
        return response.getBody().getPlaneDTO().getEvents().isCanJoin();
    }

    public void communicateSuccessfulLanding(long planeId, FlightEvents events, FlightState.FlightStatus status) {
        PlaneDto plane = PlaneDto.builder()
                .id(planeId)
                .status(status)
                .events(events)
                .build();
        Message response = Message.successfullyLanding(plane);
        messageService.send(response);
    }

    public FlightEvents requestPermissionForLanding(long id, FlightEvents events, Coordinates coordinates, FlightState.FlightStatus status) {
        PlaneDto plane = PlaneDto.builder()
                .id(id)
                .events(events)
                .status(status)
                .coordinates(coordinates)
                .build();
        messageService.send(Message.requestToLand(plane));
        Message response = messageService.getMessage();
        return response.getBody().getPlaneDTO().getEvents();
    }

    public Message handlePositionUpdate(long id, Coordinates coordinates, FlightDynamics dynamics, FlightEvents events, FlightState.FlightStatus status) {
        PlaneDto plane = PlaneDto.builder()
                .id(id)
                .coordinates(coordinates)
                .status(status)
                .events(events)
                .dynamics(dynamics)
                .build();
        Message request = Message.positionUpdate(plane);
        messageService.send(request);
        return messageService.getMessage();
    }

    public void notifyControlTowerAboutLowFuelLevel(long id, Coordinates coordinates, FlightDynamics dynamics, FlightState.FlightStatus status) {
        PlaneDto plane = PlaneDto.builder()
                .id(id)
                .coordinates(coordinates)
                .status(status)
                .dynamics(dynamics)
                .build();
        Message request = Message.positionUpdate(plane);
        messageService.send(request);
    }
}
