package com.baran.daniel.service.communication;

import com.baran.daniel.communication.Message;
import com.baran.daniel.utility.JsonParser;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

@Slf4j
public class MessageService {

    private static final Logger LOG = LoggerFactory.getLogger("MessageService");
    private final PrintWriter out;
    private final BufferedReader in;

    public MessageService(PrintWriter out, BufferedReader in) {
        this.out = out;
        this.in = in;
    }

    public Message getMessage() {
        String message;
        try {
            message = in.readLine();
            return JsonParser.from(message, Message.class);
        } catch (IOException e) {
            LOG.error("Exception occurred while processing a message: {}", e.getMessage());
        }
        return null;
    }

    public void send(Message message) {
        String json = JsonParser.to(message);
        out.println(json);
    }
}
