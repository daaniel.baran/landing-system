package com.baran.daniel.service.communication;

import com.baran.daniel.config.PropertyLoader;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import static java.util.Objects.nonNull;

@Getter
public class Communication {

    private static class Constants {
        private static final int PORT = PropertyLoader.getPort();
        private static final String HOST = PropertyLoader.getHost();
    }

    private static final Logger LOG = LoggerFactory.getLogger("Communication");
    private final MessageService messageService;

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private boolean isConnected;

    public Communication() {
        this.isConnected = this.startConnection();
        this.messageService = new MessageService(out, in);
    }

    public boolean startConnection() {
        try {
            this.clientSocket = new Socket(Constants.HOST, Constants.PORT);
            this.out = new PrintWriter(clientSocket.getOutputStream(), true);
            this.in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            LOG.info("[PLANE] connection CREATED!");
        } catch (IOException e) {
            LOG.error("[PLANE] Connection REFUSED, Airport not responding! Error message: {}", e.getMessage());
            return false;
        }
        return true;
    }

    public void stopConnection() {
        try {
            if (nonNull(this.in)) this.in.close();
            if (nonNull(this.out)) this.out.close();
            if (nonNull(this.clientSocket)) this.clientSocket.close();
            LOG.info("[PLANE] connection CLOSED!");
            this.isConnected = false;
        } catch (IOException e) {
            LOG.error("[PLANE] connection NOT CLOSED, because of exception: {}", e.getMessage());
        }
    }
}
