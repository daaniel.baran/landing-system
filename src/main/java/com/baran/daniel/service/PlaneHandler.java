package com.baran.daniel.service;

import com.baran.daniel.communication.Message;
import com.baran.daniel.communication.MessageBody;
import com.baran.daniel.dto.PlaneDto;
import com.baran.daniel.model.Coordinates;
import com.baran.daniel.model.FlightEvents;
import com.baran.daniel.model.LandingDetails;
import com.baran.daniel.model.CollisionAvoidance;
import com.baran.daniel.service.communication.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Optional;

import static com.baran.daniel.communication.Message.*;
import static com.baran.daniel.model.FlightState.FlightStatus.ENDED;
import static java.util.Objects.nonNull;

public class PlaneHandler implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger("PlaneHandler");

    private final MessageService messageService;
    private final ControlTower controlTower = ControlTower.getInstance();

    public PlaneHandler(Socket socket) throws IOException {
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.messageService = new MessageService(out, in);
    }

    @Override
    public void run() {
        Message planeRequest;
        while (nonNull(planeRequest = messageService.getMessage())) {
            Message airportResponse = handlePlaneRequest(planeRequest);
            if (nonNull(airportResponse)) {
                messageService.send(airportResponse);
            }
        }
    }

    private Message handlePlaneRequest(Message request) {
        Message.MessageType type = request.getMessageType();
        MessageBody body = request.getBody();

        PlaneDto plane = body.getPlaneDTO();
        long planeId = plane.getId();

        Coordinates coordinates = body.getPlaneDTO().getCoordinates();
        FlightEvents events = plane.getEvents();

        switch (type) {
            case NEW_PLANE -> {
                boolean canJoin = controlTower.handlePermissionToJoin(plane);
                plane.getEvents().setCanJoin(canJoin);

                LOG.info("NEW PLANE |{}| on the radar. @Position |{}| ", planeId, coordinates);
                return requestToJoin(plane);
            }
            case POSITION_UPDATE -> {
                controlTower.updatePosition(plane);
                Optional<CollisionAvoidance> collisionAvoidance = controlTower.checkForCollision(plane);

                if (collisionAvoidance.isPresent()) {
                    events.setCollisionAvoidance(collisionAvoidance.get());
                    events.setCurrentStep(2);
                    return possibleCollision(plane);
                }
                return positionUpdate(plane);
            }
            case PERMISSION_TO_LAND -> {
                int corridorId = events.getLandingDetails().getCorridorId();
                boolean hasLandingPermission = controlTower.checkCorridorAvailability(planeId, corridorId);
                if (hasLandingPermission) {
                    LandingDetails landingDetails = events.getLandingDetails();
                    int approaches = events.getLandingDetails().getApproaches();

                    landingDetails.setHasPermission(true);
                    landingDetails.setApproaches(approaches + 1);
                    plane.setEvents(events);
                    LOG.info("Plane |{}| CAN land @Corridor {}", planeId, corridorId);
                }
                return requestToLand(plane);
            }
            case LANDED -> {
                int corridorId = events.getLandingDetails().getCorridorId();

                controlTower.removePlaneFromMonitor(planeId, corridorId, ENDED);
                LOG.info("Plane |{}| LANDED @Corridor {}!", planeId, corridorId);
                return null;
            }
            default -> {
                return errorMessage("There was an error with your message");
            }
        }
    }
}


