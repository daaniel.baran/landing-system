package com.baran.daniel.service;

import com.baran.daniel.communication.Message;
import com.baran.daniel.config.PropertyLoader;
import com.baran.daniel.dto.PlaneDto;
import com.baran.daniel.model.CollisionAvoidance;
import com.baran.daniel.model.FlightDynamics;
import com.baran.daniel.model.FlightEvents;
import com.baran.daniel.model.FlightPlan;
import com.baran.daniel.model.FlightState;
import com.baran.daniel.model.Waypoint;
import com.baran.daniel.observer.PlaneObserver;
import com.baran.daniel.service.communication.PlaneCommunication;
import com.baran.daniel.utility.CoordinatesHelper;
import com.baran.daniel.utility.FlightPlanHelper;
import com.baran.daniel.utility.LogsHelper;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.baran.daniel.model.FlightState.FlightStatus.*;
import static java.util.Objects.nonNull;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

@Getter
@Setter
@ToString
public class Plane implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger("Plane");
    private static final int TIME_DELAY = PropertyLoader.getTimeDelayInMillis();
    private static final int WAYPOINT_RADIUS = PropertyLoader.getWaypointRadius();

    private final long id;
    private final PlaneCommunication planeCommunication;
    private PlaneObserver observer;
    private FlightState state;
    private FlightDynamics dynamics;
    private FlightPlan flightPlan;
    private FlightEvents events;

    public Plane(long id) {
        this.id = id;
        this.planeCommunication = new PlaneCommunication();
        this.dynamics = new FlightDynamics();
        this.state = new FlightState();
        this.flightPlan = new FlightPlan(state.getCoordinates());
        this.events = new FlightEvents();
        String logMessage = LogsHelper.logPlanePosition(this);
        LOG.info("{}", logMessage);
    }

    @Override
    public void run() {
        try {
            while (planeCommunication.getCommunication().isConnected()) {
                updateAnimationDisplay();
                MILLISECONDS.sleep(TIME_DELAY);

                if (state.getStatus() == NEW) {
                    boolean canJoin = planeCommunication.requestPermissionToJoin(id, events, state.getCoordinates(), state.getStatus());
                    if (canJoin)
                        state.setStatus(FLYING);
                    else
                        handleRejected(id);
                } else if (state.getStatus() == FLYING) {
                    fly();
                } else if (state.getStatus() == ENDED || state.getStatus() == CRASHED) {
                    break;
                } else
                    throw new IllegalStateException("Unexpected value: " + state.getStatus());
            }
        } catch (InterruptedException e) {
            LOG.error("InterruptedException {}", e.getMessage());
            Thread.currentThread().interrupt();
        } finally {
            //TODO clear all resources
            planeCommunication.getCommunication().stopConnection();
//            observer.removePlane();
        }
    }

    private void fly() {
        adjustHeading();
        adjustVelocity();
        adjustHeightChange();
        handlePossibleCollision();
        updatePosition();
        handleWaypointApproach();
        updateFuelConsumption();
    }

    private void adjustVelocity() {
        if (isVelocityAdjustmentNecessary())
            dynamics.updateVelocityForCollisionAvoidance(events.getCollisionAvoidance().adjustedVelocity());
        else
            dynamics.updateVelocityBasedOnWaypoint(flightPlan.getCurrentWaypoint());
    }

    private void handleWaypointApproach() {
        Waypoint waypoint = flightPlan.getCurrentWaypoint();
        double distanceToWaypoint = CoordinatesHelper.distanceBetweenTwoCoordinates2D(state.getCoordinates(), waypoint.getCoordinates());
        if (distanceToWaypoint <= WAYPOINT_RADIUS) {
            if (waypoint.isCheckpoint()) {
                events.getLandingDetails().setCorridorId(FlightPlanHelper.getCorridorFromWaypoint(waypoint));
                this.events = planeCommunication.requestPermissionForLanding(id, events, state.getCoordinates(), state.getStatus());
            } else if (distanceToWaypoint <= dynamics.getVelocity() && waypoint.isRunway()) {
                handleSuccessfulLanding();
            }
            if (state.getStatus() != ENDED && !waypoint.isRunway())
                flightPlan.updateCurrentWaypoint(events);
        }
    }

    private void handlePossibleCollision() {
        CollisionAvoidance collisionAvoidance = events.getCollisionAvoidance();
        if (collisionAvoidance != null) {
            LOG.info("!!! PLANE |{}| POSITION ADJUSTED TO AVOID COLLISION", id);
            int currentStep = events.getCurrentStep();
            if (currentStep <= 2) {
                events.setCurrentStep(--currentStep);
                if (currentStep == 0)
                    events.setCollisionAvoidance(null);
            }
        }
    }

    public void setObserver(PlaneObserver observer) {
        this.observer = observer;
    }

    public void removeObserver() {
        this.observer = null;
    }

    public void updateAnimationDisplay() {
        if (nonNull(observer)) {
            PlaneDto dto = PlaneDto.builder()
                    .id(id)
                    .dynamics(dynamics)
                    .coordinates(state.getCoordinates())
                    .status(state.getStatus())
                    .build();
            observer.updatePlane(dto);
        }
    }

    private void updatePosition() {
        state.getCoordinates()
                .updateCoordinatesByHeading(dynamics.getVelocity(), dynamics.getCurrentHeading(), dynamics.getAltitudeChange());
        String logMessage = LogsHelper.logPlanePosition(this);

        Message response = planeCommunication.handlePositionUpdate(id, state.getCoordinates(), dynamics, events, state.getStatus());
        events = response.getBody().getPlaneDTO().getEvents();
        LOG.info("{}", logMessage);
    }

    private void adjustHeightChange() {
        if (isAltitudeAdjustmentNecessary())
            dynamics.updateHeightChangeForCollisionAvoidance(events.getCollisionAvoidance().adjustedAltitude());
        else
            dynamics.updateHeightChange(state.getCoordinates(), flightPlan.getCurrentWaypoint());
    }

    private void adjustHeading() {
        if (isHeadingAdjustmentNecessary())
            dynamics.updateHeadingForCollisionAvoidance(events.getCollisionAvoidance().adjustedHeading());
        else
            dynamics.updateHeadingByWaypoint(state.getCoordinates(), flightPlan.getCurrentWaypoint());
    }

    private boolean isAltitudeAdjustmentNecessary() {
        return nonNull(events.getCollisionAvoidance()) && nonNull(events.getCollisionAvoidance().adjustedAltitude());
    }

    private boolean isVelocityAdjustmentNecessary() {
        return nonNull(events.getCollisionAvoidance()) && nonNull(events.getCollisionAvoidance().adjustedVelocity());
    }

    private boolean isHeadingAdjustmentNecessary() {
        return nonNull(events.getCollisionAvoidance()) && nonNull(events.getCollisionAvoidance().adjustedHeading());
    }

    private void updateFuelConsumption() {
        dynamics.consumeFuel();
        if (dynamics.isLowFuelLevel()) {
            planeCommunication.notifyControlTowerAboutLowFuelLevel(id, state.getCoordinates(), dynamics, state.getStatus());
            LOG.warn("ATTENTION!! Low fuel level on plane {}", id);
        }
        if (dynamics.isOutOfFuel())
            state.setStatus(CRASHED);
    }

    private void handleSuccessfulLanding() {
        flightPlan.updateCurrentWaypoint(events);
        planeCommunication.communicateSuccessfulLanding(id, events, state.getStatus());
        LOG.info("PLANE |{}| LANDED SUCCESSFULLY", id);
        state.setStatus(ENDED);
        removeObserver();
    }

    private void handleRejected(long planeId) {
        state.setStatus(REJECTED);
        LOG.error("PLANE |{}| REJECTED", planeId);
        Thread.currentThread().interrupt();
        planeCommunication.getCommunication().stopConnection();
    }
}