package com.baran.daniel.service;

import com.baran.daniel.config.PropertyLoader;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.Objects.isNull;

public class Airport {

    private static final int DEFAULT_PORT = PropertyLoader.getPort();
    private static final Logger LOG = LoggerFactory.getLogger("Airport");
    private static Airport airport;
    private final ExecutorService executorService;

    @Setter
    private boolean isAirportRunning;

    private Airport() {
        this.isAirportRunning = true;
        executorService = Executors.newFixedThreadPool(PropertyLoader.getMaxCapacity());
    }

    public static Airport getInstance() {
        if (isNull(airport))
            airport = new Airport();
        return airport;
    }

    private void start() {
        try (ServerSocket airportSocket = new ServerSocket(DEFAULT_PORT)) {
            LOG.info("AIRPORT started, ready to welcome planes...!");
            while (isAirportRunning) {
                Socket newPlane = airportSocket.accept();
                executorService.execute(new PlaneHandler(newPlane));
            }
        } catch (IOException e) {
            LOG.error("Exception occurred while trying to connect a new plane: {}", e.getMessage());
            stopAirport();
        }
    }

    private void stopAirport() {
        this.isAirportRunning = false;
        this.executorService.shutdown();
    }

    public static void main(String[] args) {
        Airport airport = Airport.getInstance();
        airport.start();
    }
}
