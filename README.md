Project is about creating a system to manage airport's landing system of 100 planes.
Postgres db, and communication with sockets. 

NOTES & STATUS:

- RECOGNIZED ISSUES:
    - [x] Flight And Turning,
        - [x]  initial position and next step to get to ON_WAITING_LINE,
        - [x]  After turn catching next line is a goal now
        - [x] Communication airport <-> plane
            - [x] Send proper Request, RequestType,
            - [x] Than has to be converted to Json,
            - [x] Than parsed from Json and read by traffic control,
            - [x] Process it, and replay to Plane, 
            - [x] Giving a permission to land,
                - [x] Plane when close to corridors sends an query to Airport, 
                    - [x] waits for response,
                    - [x] If true,
                        - [x] Turns once, and fly one  
                - [x] Situation happens twice as there are two corridors,
        - [x] Database queries to write
            - [x] New
            - [x] Update status:
                - [x] Landed
                    - [x] Set end time
                - [ ] Collision
                    - [ ] Update status
                    - [ ] Set end time
- [x] OBJECTS:
    - [x] Plane = CLIENT,
        - [x] Simplify traffic, 
            - [x] 1s = change of 1 Cube = 10m,
        - [x] Is a point,
        - [x] Fuel:  for 3h of flight,
            - [x] If plane runs off the fuel  -> collision,
            - [x] 10_800_000 milliseconds = max time,
        - [x] Initial location: 
            - [x] Coordinates XYZ,
            - [x] Random on the edge of board , 
            - [x] Height from 4km,
            - [x] Outside the corridors,
    - [x] Server = AIRPORT,
        - [x] Airport has to landing corridors,,
    - [ ] Traffic Control 
        - [x] Collision if 2 planes are closer than 10m,
            - [x] Monitor all planes position,
                - [ ] Warn and force planes to change route,
        - [x] Airport area => 10km x 10km x 5km (height),
        - [ ] Planes to wait for landing ? Or FIFO.
            - [ ] Queue for planes, 
            - [x] Poruszają się wówczas ruchem zbliżonym do okrężnego, gęsiego
                - [x] Opracować strefy/linie do oczekiwania,
                    - [x] 1km from border +/- 50m,
            - [x] Two gates to let planes into corridors,
        - [x] Block or release corridors.
        - [x] Max 100 planes on the board,
            - [x] Above the limit - information about exceeded limit and out of the radar.
    - [x] Landing line (2km),
    - [x] Corridors to land:
        - [x] From 3km height to 0,

—————————————————————

- [ ] FLIGHT ALGORYTHM:
    - [x] Phase 1 New object:
        - [x] Create plany on the edge of airport:
            - [x] Get random position,
            - [x] Set direction,
        - [x] Fly from edge to waiting line,
            - [x] Move x/y/z 10/s, 
            - [x] Continue until closer than 20m, 
                - [x] Turn left  ex. WEST -> SOUTH_WEST,
                - [x] Change of direction,
    - [x] Phase 2 Waiting (1km from edge):
        - [x] Plany in WAITING ZONE (50m line),
        - [x] Follow the line in waiting zone,
        - [x] LANDING GATES: 
            - [x] On the GATE 1 it checks if is it free,
            - [x] If YES -> phase 3 (landing)
            - [x] IF NOT flies to GATE 2 and repeats, if busy go back to waiting,
    - [x] Phase 3 Landing:
        - [x] Turn right to the corridor,
        - [x] Get lower to level 0m,
            - [x] Calculate distance and speed for landing,
        - [x] Height has to be 0m!,
            - [x] Otherwise crash!
            - [x] Landing successful, remove plane from board
        - [x] Commands 
        - [x] Airport -> Plane sends responses

—————————————————————

- [ ] DATABASE, 
    - [x] Postgres + JOOQ,
    - [ ] What should be collected by db:
        - [x] New connection,
        - [x] Successful landing,
        - [ ] Collisions
    - [ ] DATABASE STRUCTURE:
        - [x] PLANE
            - [x] ID, STATUS, START, END
        - [x] EVENT
            - [x] ID, STATUS, TIMESTAMP

